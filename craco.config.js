const CracoAntDesignPlugin = require("craco-antd");
const CracoLessPlugin = require("craco-less");
const cracoPluginStyleResourcesLoader = require("craco-plugin-style-resources-loader");
const path = require("path");
const webpack = require("webpack");
const tailwindcss = require('tailwindcss');

module.exports = {

  "stories": [
    "../src/**/*.stories.mdx",
    "../src/**/*.stories.@(js|jsx|ts|tsx)"
  ],
  "addons": [
    "@storybook/addon-links",
    "@storybook/addon-essentials",
    "@storybook/preset-create-react-app"
  ],
  style: {
    postcss: {
      plugins: [
        require("tailwindcss"),
        require("autoprefixer"),
      ],
    },
  },
  plugins: [
    // new webpack.ProvidePlugin({
    //   process: "process/browser",
    // }),
    // links
    //https://stackoverflow.com/questions/41359504/webpack-bundle-js-uncaught-referenceerror-process-is-not-defined
    //https://github.com/facebook/create-react-app/issues/12212
    //https://stackoverflow.com/questions/70368760/react-uncaught-referenceerror-process-is-not-defined/71151846#71151846
    { plugin: CracoAntDesignPlugin },
    {
      plugin: CracoLessPlugin,
      options: {
        lessLoaderOptions: {
          lessOptions: {
            javascriptEnabled: true,
          },
        },
      },
    },
    {
      plugin: cracoPluginStyleResourcesLoader,
      options: {
        patterns: path.join(__dirname, "./src/app.theme.less"),
        /*
            Please enter supported CSS processor type
            1. if u use css processor，please type css string
            2. if u use less processor，please type less string
            3. if u use sass or scss processor，please type sass or scss string，Choose one of the two
            4. if u use stylus processor，please type stylus string
        */
        styleType: "less",
      },
    },
    {
      plugin: tailwindcss,
      options: {
        config: './tailwind.config.js'
      }
    }
  ],
};

