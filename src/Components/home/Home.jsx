import React, { useState } from "react"
import AboutCard from "../about/AboutCard"
import Hblog from "./Hblog"
import HAbout from "./HAbout"
import Hero from "./hero/Hero"
import Hprice from "./Hprice"
import "./Home.css"
import Testimonal from "./testimonal/Testimonal"
import Panier from "../../Panier"
import Footer from "../container/Footer";
import { useNavigate } from "react-router-dom";
import { FaClipboardCheck, FaUser, FaSignOutAlt } from 'react-icons/fa';



const Home = () => {

  const navigate = useNavigate();

  const quizpage = () => {
    navigate('/Quiz')
  }
  const profilepage = () => {
    navigate('/Instructor')
  }



  return (
    <>


      <nav className="flex justify-between items-center py-4 px-6 bg-transparent">
        <div className="flex items-center flex-grow">
          <h1 className="text-2xl font-bold text-gray-800 text-Teal">UPSKILL LABS</h1>
        </div>
        <div className="flex items-center justify-center space-x-6">
          {/* Add your additional navbar elements here */}
          <h1 onClick={() => quizpage()} className="text-gray-800 hover:text-gray-600">
            <FaClipboardCheck className="inline-block mr-2" />Assessment
          </h1>
          <h1 onClick={() => profilepage()} className="text-gray-800 hover:text-gray-600">
            <FaUser className="inline-block mr-2" />Profile
          </h1>
          <h1 className="text-gray-800 hover:text-gray-600">
            <FaSignOutAlt className="inline-block mr-2" />Logout
          </h1>
        </div>
      </nav>











      <Hero />
      <AboutCard />
      <Panier />

      <HAbout />
      <Testimonal />
      <Hblog />
      <div class="flex justify-center items-center h-screen">
        <Hprice />
      </div>
      <Footer />

    </>
  )
}

export default Home
