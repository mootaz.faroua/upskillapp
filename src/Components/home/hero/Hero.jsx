import React from "react"
import Heading from "../../common/heading/Heading"
import "./Hero.css"

const Hero = () => {
  return (
    <>
      <section className='hero'>
        <div className='container'>
          <div className='row'>
            <Heading subtitle='WELCOME TO ACADEMIA' title='Best Online Education Expertise' />
           
            <div class="flex justify-start items-center">
              <button class="bg-Teal hover:bg-blue-700 text-white font-bold py-2 px-4 rounded">
                GET STARTED
              </button>
              <div class="w-4"></div>
              <button class="bg-white hover:bg-blue-700 text-Teal font-bold py-2 px-4 rounded">
                Button
              </button>
            </div>

          </div>
        </div>
      </section>
      <div className='margin'></div>
    </>
  )
}

export default Hero
