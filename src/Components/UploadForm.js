import React, { useState } from "react";
import axios from "axios";
import { selectSessionUser } from "../topics/Session/slice";
import { useSelector } from "react-redux";
import { baseURL } from "../constants";
import Footer from "./container/Footer";
import {  selectUserRole } from "../topics/Session/slice";



const UploadForm = ({ getAllMedias, courseId }) => {
  const [name, setName] = useState("");
  const [resume, setResume] = useState("");
  const currentUser = useSelector(selectSessionUser);
  const [User_id, setUser_id] = useState(currentUser._id);
  const userRole = useSelector(selectUserRole);


  const [videos, setVideos] = useState([]);


  // if (userRole !== 'instructor') {
  //   alert("You need to be an instructor ");
  //   return null; // Return null or any other component to prevent rendering for non-instructors
  // }
  const handleSubmit = (e) => {
    e.preventDefault();

    let formData = new FormData();
    for (let key in videos) {
      formData.append("videos", videos[key]);
    }

    formData.append("name", name);
    formData.append("resume", resume);
    formData.append("User_id", User_id);
    formData.append("course_id", courseId);


    axios
      .post(`${baseURL}/api/v1/media/create`, formData)
      .then((success) => {
        getAllMedias();
        alert("Submitted successfully");
      })
      .catch((error) => {
        console.log(error);
        alert("Error happened!");
      });
  };


  return (
    <div className="max-w-md mx-auto   py-20">

      <form onSubmit={handleSubmit}>
        <div class="relative z-0 w-full mb-6 group py-5">
          <input type="text" name="name"
            id="name" class="block py-2.5 px-0 w-full text-sm text-gray-900 bg-transparent border-0 border-b-2 border-gray-300 appearance-none dark:text-white dark:border-gray-600 dark:focus:border-blue-500 focus:outline-none focus:ring-0 focus:border-blue-600 peer" placeholder=" Title "   onChange={(e) => setName(e.target.value)}
            required />
          <label for="floating_email" class="peer-focus:font-medium absolute text-sm text-gray-500 dark:text-gray-400 duration-300 transform -translate-y-6 scale-75 top-3 -z-10 origin-[0] peer-focus:left-0 peer-focus:text-blue-600 peer-focus:dark:text-blue-500 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-6"></label>
        </div>
        <div class="relative z-0 w-full mb-6 group py-5">

          <label for="floating_password" class="peer-focus:font-medium absolute text-sm text-gray-500 dark:text-gray-400 duration-300 transform -translate-y-6 scale-75 top-3 -z-10 origin-[0] peer-focus:left-0 peer-focus:text-blue-600 peer-focus:dark:text-blue-500 peer-placeholder-shown:scale-100 peer-placeholder-shown:translate-y-0 peer-focus:scale-75 peer-focus:-translate-y-6"></label>
          <textarea
            name="resume"
            id="resume"
            className="border border-gray-300 rounded-lg w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:ring-2 focus:ring-blue-500 focus:border-transparent"
            placeholder=" Description "
            onChange={(e) => setResume(e.target.value)}
            required
          />
        </div>
       
        <div>
          <label htmlFor="videos" className="block text-gray-700 font-bold mb-2">
           
          </label>
          <div className="flex items-center py-5">
            <input
              type="file"
              name="videos"
              id="videos"
              multiple
              className="border border-gray-300 rounded-lg py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:ring-2 focus:ring-blue-500 focus:border-transparent"
              accept=".mp4, .mkv, .jpeg"
              onChange={(e) => {
                setVideos(e.target.files);
              }}
              required
            />
  
            {videos.length > 0 &&
              Array.from(videos).map((video, index) => (
                <div key={index} className="ml-2">
                  <span>{video.name}</span>
                </div>
              ))}
          </div>
          </div>
          <br/>
        
        <button type="submit" class="text-white bg-Teal hover:bg-green-900 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm w-full sm:w-auto px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800">Submit</button>
      </form>


    </div>
  );
};

export default UploadForm;
