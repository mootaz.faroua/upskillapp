import React ,{useState} from 'react'
import Button from './chatboat/Button'
import Chat from './chatboat/Chat'

const Chatboatmain = () => {
    const [show, setShow] = useState(false);

    const showBtn = () => {
      setShow(!show)
    }

   
  return (
    <>
    <Button show={show} showBtn={showBtn} />
    {show && <Chat />}
  </>
  )
}

export default Chatboatmain
