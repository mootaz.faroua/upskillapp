import React, { useState } from "react";
import "../style/shoppingCart.css";
import { AiFillCloseCircle } from "react-icons/ai";
import { RiDeleteBin6Line } from "react-icons/ri";
import StripeCheckout from "react-stripe-checkout";
import axios from 'axios';
import {courses} from "../Panier";
import Swal from "sweetalert2";
import withReactContent from "sweetalert2-react-content";

function ShoppingCart({
  visibilty,
  courses,
  onProductRemove,
  onClose,
  onQuantityChange,
})


{
  const publishableKey="pk_test_51LT17fHFI0nvdg87cyTeBCteEeCJRG4TXjfEWBMRCO8rk9qBQUqbruD4CxKfjvnwMxQFJcDF4TaqCWj8OytycrkI004Y5Z1rkJ"
  const totalPrice = courses.reduce(
    (acc, course) => acc + course.price * course.count,
    0
  );

  const [stripeToken, setStripeToken] = useState(null);
  const MySwal = withReactContent(Swal)
  
const handlesuccess = ()=>{
  MySwal.fire({
    icon: 'succes',
    title: 'payment was succesfull',
    time:4000,

  })
},
handlefailure = ()=>{
  MySwal.fire({
    icon: 'error',
    title: 'payment was not succesfull',
    time:4000,

  })

}


  const handleToken = async (token) => {
	try {
	  const response = await axios({
      url:'http://localhost:4000/payment',
      method: 'post',
      data:{
      amount: courses.price *100,
      token,
      },
	
	  });
  if(response.status == 200){
    handlesuccess();
  }

	} catch (error) {
    handlefailure();
	  console.log(error);
	}
  };

  return (
    <div
      className="modal"
      style={{
        display: visibilty ? "block" : "none",
      }}
    >
      <div className="shoppingCart">
        <div className="header">
          <h2>Upskill Labs Modal</h2>
          <button className="btn close-btn" onClick={onClose}>
            <AiFillCloseCircle size={30} />
          </button>
        </div>
        <div className="cart-products">
          {courses.length === 0 && (
            <span className="empty-text">Your basket is currently empty</span>
          )}
          {courses.map((course) => (
            <div className="cart-product" key={course._id}>
              <div className="product-info">
                <h3>{course.title}</h3>
                <span className="product-price">
                  {course.price * course.count}$
                </span>
              </div>
              {/* <select
                className="count"
                value={product.count}
                onChange={(event) => {
                  onQuantityChange(product.id, event.target.value);
                }}
              >
                {[...Array(10).keys()].map((number) => {
                  const num = number + 1;
                  return (
                    <option value={num} key={num}>
                      {num}
                    </option>
                  );
                })}
              </select> */}
              <button
                className="btn remove-btn"
                onClick={() => onProductRemove(course)}
              >
                <RiDeleteBin6Line size={20} />
              </button>
            </div>
          ))}
          {courses.length > 0 && (
            <div>
              <div className="total-price font bold text-3xl">
                Total Price: {totalPrice}$
              </div>
              <StripeCheckout
                stripeKey={publishableKey}
                token={handleToken}
                amount={totalPrice * 100}
                name="Payment Modal"
                billingAddress
                shippingAddress
              >
                <button className="btn checkout-btn bg-blue-500">
                  Proceed to checkout
                </button>
              </StripeCheckout>
            </div>
          )}
        </div>
      </div>
    </div>
  );
}

export default ShoppingCart;
