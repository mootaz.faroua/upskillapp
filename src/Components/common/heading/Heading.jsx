import React from "react"

const Heading = ({ subtitle, title ,title2}) => {
  return (
    <>
      <div id='heading'className="mx-10">
        <h3 className="font-bold py-2 text-xl mt-80">{subtitle} </h3>
        <h1 className="" >{title} </h1>
        <h1 >{title2} </h1>
      
      </div>
    </>
  )
}

export default Heading
