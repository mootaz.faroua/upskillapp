// courseActions.js

import {
    DELETE_COURSE_REQUEST,
    DELETE_COURSE_SUCCESS,
    DELETE_COURSE_FAILURE
  } from '../Actions/types';
  import axios from 'axios';
  export const deleteCourseRequest = () => ({
    type: DELETE_COURSE_REQUEST
  });
  
  export const deleteCourseSuccess = (id) => ({
    type: DELETE_COURSE_SUCCESS,
    payload: id
  });
  
  export const deleteCourseFailure = (error) => ({
    type: DELETE_COURSE_FAILURE,
    payload: error
  });
  
  export const deleteCourse = (id) => async (dispatch) => {
    try {
      const response = await axios.delete(`http://localhost:4000/course/deletecourse/${id}`);
      dispatch({ type: DELETE_COURSE_SUCCESS, payload: response.data });
    } catch (error) {
      dispatch({ type: DELETE_COURSE_FAILURE, payload: error.message });
    }
  };

