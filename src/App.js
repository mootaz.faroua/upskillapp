// Packages
import React from "react";
import PropTypes from "prop-types";
import axios from "axios";
import { useNavigate } from "react-router-dom";
import { useTranslation } from "react-i18next";

// UI components
import { Layout as AntLayout, message, notification } from "antd";

// Style
import "./App.less";

function App({ children }) {
  /* ********************************** HOOKS ********************************* */

  const navigate = useNavigate();

  // Localization
  const { t } = useTranslation(["common"]);

  // Handle expired tokens wherever they arise
  axios.interceptors.response.use(
    (response) => {
      return response;
    },
    (error) => {
      const status = error?.response?.status;
      console.log("test",error?.response);
      if (status) {
        switch (status) {
          
          case 401:
            navigate("/");
            notification.error({
              message: t("common:auth.expiredSession"),
              placement: "topRight",
            });
            break;
          case 404:
          {              navigate("/not-found", { state: { reason: "resource" } });
            break;}
            case 400:
            {      
              message.error(error?.response?.data?.message);
              break;}
          default:
            break;
        }
      } else if (error) {
        notification.error({
          message: `${t("common:unknownError")} : ${error}`,
          placement: "topRight",
        });
      }
      return Promise.reject(error);
    }
  );
  return <AntLayout id="app">{children}</AntLayout>;
}

App.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(React.Element).isRequired,
    PropTypes.node,
  ]),
};

App.defaultProps = {
  children: [],
};

export default App;
