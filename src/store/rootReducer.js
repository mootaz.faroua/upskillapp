import { combineReducers } from "redux";
import Session from "../topics/Session/slice";
import Layout from "../topics/layouts/slice";
import JobOffers from "../topics/recruterPage/slice";



export default combineReducers({ Session, Layout, JobOffers });
