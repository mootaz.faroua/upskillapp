import React from 'react';

import { PhoneIcon, ArrowSmRightIcon } from '@heroicons/react/outline';
import { ChipIcon, SupportIcon } from '@heroicons/react/solid'
import support2Img from '../assets/support2.png'
import support3Img from '../assets/support3.png'
import support4Img from '../assets/support4.png'
import './support.css'


const Support = () => {  
    return (
        <div name='support' className='w-full bg-gray-100 py-20 '>
            <div className='p-8 '> 
            <p className='text-6xl font-bold text-center text-indigo-600 mt-2'>By your side </p>
            <p className='text-6xl font-bold text-center text-indigo-600 mt-2'> every step of the way</p>
            <p className='text-2xl text-center text-gray-500 mt-2'>Here we are present to help you through your career </p>
            <p className='text-2xl text-center text-gray-500 mt-2 '>from the level of your choice and all the way up !</p>
            </div>
            <div className='max-w-[1240px] mx-auto text-white relative '>
              
                <div className='grid grid-cols-1 lg:grid-cols-3 relative gap-x-2 gap-y-16 px-20 pt-2  sm:pt-1 text-black '>
               
                    <div >
                        <div className='p-8'>
                            <img className='support4  h-55 w-40 justify-center items-center' src={support4Img} alt="/" />
                            <p className='text-3xl font-bold  text-indigo-600 mt-2'>Stand out in job</p>
                            <p className='text-3xl font-bold  text-indigo-600 mt-2'>applications</p>
                            <p className='text-gray-600 mt-4'>Let us help you build the</p>
                            <p className='text-gray-600 mt-2'>resume that highlights your</p>
                            <p className='text-gray-600 mt-2'>skills and gets you the</p>
                            <p className='text-gray-600 mt-2'>dream job come true.</p>
                      

                        </div>

                    </div>
                    <div >
                        <div className='p-8'>
                            <img className=' h-50 w-40  justify-center items-center' src={support3Img} alt="/" />
                            <p className='text-3xl font-bold  text-indigo-600 mt-2'>Our experts are </p>
                            <p className='text-3xl font-bold  text-indigo-600 mt-2'>just one click</p>
                            <p className='text-3xl font-bold  text-indigo-600 mt-2'> away</p>
                            <p className='text-gray-600 mt-2'>Our mentors are available </p>
                            <p className='text-gray-600 mt-2'> to help you through your </p>
                            <p className='text-gray-600 mt-2'> journey with the right kick- </p>
                            
                        </div>
                    </div>
                    <div >
                        <div className='p-8'>
                            <img  className='support2 h-50 w-40  flex justify-center items-center' src={support2Img} alt="/"  />
                            <p className='text-3xl font-bold  text-indigo-600 '>Showcase your</p>
                            <p className='text-3xl font-bold  text-indigo-600 mt-2'> projects </p>
                            <p className='text-gray-600 mt-4'>The perfect place to showcase your work and get reviews, offers and much more.
                           </p>
                            <p className='text-gray-600 mt-1'> showcase your work and get reviews
                           </p>
                            <p className='text-gray-600 mt-1'> much more.
                           </p>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    );
};

export default Support;