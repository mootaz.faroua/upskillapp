import React from 'react'
import './footer.css'
import {
    FaFacebook,
    FaGithub,
    FaInstagram,
    FaTwitter,
    FaTwitch,
} from 'react-icons/fa'

const Footer = () => {
  return (
    <div className='footer w-full mt-2 bg-slate-800 text-gray-300 py-y px-2'>
        {/* <div className='max-w-[1240px] mx-auto grid grid-cols-2 md:grid-cols-6 border-b-2 border-gray-600 py-8'>
            <div className='col-span-2 pt-8 md:pt-2'>
                <h6 className='font-bold uppercase pt-2  text-black'>Solutions</h6>
                <ul>
                    <li className='py-1 text-white'>Marketing</li>
                    <li className='py-1 text-white'>Analytics</li>
                    <li className='py-1 text-white'>Commerce</li>
                    <li className='py-1 text-white'>Data</li>
                    <li className='py-1 text-white'>Cloud</li>
                </ul>
            </div>
         
          
            <div className='col-span-2 pt-8 md:pt-2'>
                <p className='font-bold uppercase'>Subscribe to our newsletter</p>
                <p className='py-4 text-white'>The latest news, articles, and resources, sent to your inbox weekly.</p>
                <form className='flex flex-col sm:flex-row'>
                    <input className='w-full p-2 mr-4 rounded-md mb-4' type="email" placeholder='Enter email..'/>
                    <button className='p-2 mb-4 font-bold text-red-500'>Subscribe</button>
                </form>
            </div>
        </div> */}

        <div className='flex flex-col max-w-[1240px] px-2 py-4 mx-auto justify-between sm:flex-row text-center text-gray-500'>
        <p className='py-4'>2022 Workflow, LLC. All rights reserved</p>
        <div className='flex justify-between sm:w-[300px] pt-4 text-2xl'>
            <FaFacebook />
            <FaInstagram />
            <FaTwitter />
        </div>
        </div>
    </div>
  )
}

export default Footer