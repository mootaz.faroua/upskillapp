import React from 'react'
import PcImg from '../assets/Pc.jpeg'
import './About.css'
const Aboutt = () => {
    return (
        <div name='about' className='aboutt w-full my-20 py-20'>
              <div >
                <div >
                    
                <img className='float-right ' src={PcImg}/>
                    <div className='text-center' >
                    
                        <p className=' text-3xl font-bold text-center'> Need the help of </p>  <p className=' text-center text-3xl text-blue-500 font-bold'> an Expert ? </p>
                        <p className=' text-3xl font-bold text-center'>good enough?</p>
                        <p className='text-gray-400 text-center'>Explore our list of certified mentors. Experts who can help you advance</p>
                        <p className='text-gray-400 text-center'>and provide the best coaching.</p>
                        <button class="rounded-full text-xs text-white  bg-indigo-500 py-2 px-8 font-bold mt-5">Find your match</button>
                        

                    </div>

                </div>
             
            </div>
        </div>
    )
}
export default Aboutt;