import React from 'react'

import Navbar from './HomepageT/Navbar'
import Hero from './HomepageT/Hero'
import Support from './HomepageT/Support'
import AllInOne from './HomepageT/AllInOne'
import About from './HomepageT/About'
import Footer from './HomepageT/Footer'
import Aboutt from './HomepageT/Aboutt'
import Userfeedback from './HomepageT/Userfeedback'





export default function Homepaget() {
  return (
    <div>
      <Navbar/>
      <Hero/>
      <Support/>
      <About/>
      <Aboutt/>
      <AllInOne/>
      <Userfeedback/>
      <Footer/>
    
    </div>
  )
}
