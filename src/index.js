/* -------------------------------------------------------------------------- */
/*                                Dependencies                                */
/* -------------------------------------------------------------------------- */

// React depedencies
import React, { Suspense } from "react";
import ReactDOM from "react-dom/client";
import { BrowserRouter } from "react-router-dom";
// import { Online, Offline } from "react-detect-offline";

// Store dependencies
import { Provider as StoreProvider } from "react-redux";
import { PersistGate as PersistenceProvider } from "redux-persist/integration/react";
import { persistStore } from "redux-persist";

// Networking and routingcd
import axios from "axios";

// UI lib components
import { ConfigProvider as AntConfigProvider } from "antd";
import frFR from "antd/lib/locale/fr_FR";
import { I18nextProvider } from "react-i18next";

// Helpers & utils
// import store from "./store";
import * as serviceWorker from "./serviceWorker";
import { getSessionToken } from "./topics/Shared/helpers/auth";

// UI local components
import Router /*, { OfflineRouter } */ from "./router";
import Loader from "./topics/Shared/UIkit/Loader";

/* -------------------------------------------------------------------------- */
/*                               Loading modules                              */
/* -------------------------------------------------------------------------- */

// Load i18n configuration
import i18n from "./i18n";

// Load style

import "./index.css";
import "./app.theme.less";
import store from "./store";
// import "./antd.overrides.less";

// Handler to load API request driver with session token stored in local storage
// after rehydration
const loadAPIConfig = () => {
  axios.defaults.headers.common.Authorization = `Bearer ${getSessionToken()}`;
};

// Load store from local storage
const persistor = persistStore(store);

/* -------------------------------------------------------------------------- */
/*                                App rendering                               */
/* -------------------------------------------------------------------------- */

const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(
  <React.StrictMode>
    <StoreProvider store={store}>
      <PersistenceProvider
        persistor={persistor}
        onBeforeLift={() => loadAPIConfig()}
        loading={null}
      >
        <AntConfigProvider locale={frFR}>
          <Suspense fallback={<Loader />}>
            <I18nextProvider i18n={i18n}>
              <BrowserRouter>
                {/* <Online> */}
                <Router />
                {/* </Online> */}
                {/* <Offline>
                  <OfflineRouter />
                </Offline> */}
              </BrowserRouter>
            </I18nextProvider>
          </Suspense>
        </AntConfigProvider>
      </PersistenceProvider>
    </StoreProvider>
  </React.StrictMode>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
serviceWorker.unregister();
