/* -------------------------------------------------------------------------- */
/*                                Dependecies                                */
/* -------------------------------------------------------------------------- */

import React from "react";
import { Layout, Tabs } from "antd";
import { useLocation, useNavigate } from "react-router-dom";
// local UI components
import RequestsHistory from "./RequestsHistory";
import ScheduledSessions from "./ScheduledSessions";

// Styles
import "./index.less";

// scoped components

const { Content } = Layout;
const { TabPane } = Tabs;

/* -------------------------------------------------------------------------- */
/*                                  Component                                 */
/* -------------------------------------------------------------------------- */

function MyMentoringLab() {
  const { hash } = useLocation();
  const navigate = useNavigate();
  /* -------------------------------- CALLBACKS ------------------------------- */

  function onClickTopicTab(topic) {
    navigate({ hash: topic });
  }
  /* ---------------------------- RENDERIN HELPERS ---------------------------- */

  /* -------------------------------- RENDERING ------------------------------- */

  return (
    <Content className="services-option">
      <Tabs
        key="mentoring-lab-tabs"
        defaultActiveKey={hash ? hash.replace("#", "") : "requests-history"}
        onTabClick={function cb(topic) {
          onClickTopicTab(topic);
        }}
      >
        <TabPane tab="Requests History" key="requests-history">
          <RequestsHistory />
        </TabPane>
        <TabPane tab="Scheduled sessions" key="scheduled-sessions">
          <ScheduledSessions />
        </TabPane>
      </Tabs>
    </Content>
  );
}
export default MyMentoringLab;
