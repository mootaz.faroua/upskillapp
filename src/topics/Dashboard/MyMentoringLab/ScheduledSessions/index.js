/* -------------------------------------------------------------------------- */
/*                                Dependecies                                */
/* -------------------------------------------------------------------------- */

import { Button, Col, Collapse, Row, Space } from "antd";
import axios from "axios";
import { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import { baseURL } from "../../../../constants";
import { selectSessionUser } from "../../../Session/slice";
import { format } from "date-fns";

//style
import "./index.less";
import mentorServices from "../../../Shared/Entities/mentorServices";
// scoped componenets

const { Panel } = Collapse;

/* -------------------------------------------------------------------------- */
/*                                  Component                                 */
/* -------------------------------------------------------------------------- */

function ScheduledSessions() {
  /* ---------------------------------- HOOKS --------------------------------- */
  // redux
  const currentUser = useSelector(selectSessionUser);

  // states
  const [mentorRequests, setMentorRequests] = useState([]);

  // side- effects
  useEffect(() => {
    (async function () {
      try {
        await axios({
          method: "get",
          baseURL: `${baseURL}/mentoringRequest/getMentoringRequests`,
          params: { senderId: currentUser._id, status: "accepted" },
        })
          .then((res) => {
            setMentorRequests(res.data);
          })
          .catch();
      } catch (e) {
        console.error(e);
      }
    })();
  }, []);

  /* ----------------------------- LOCAL VARIABLES ---------------------------- */

  function getRequestClassName(status) {
    switch (status) {
      case "accepted":
        return "dot-accepted";
      case "pending":
        return "dot-pending";
      case "rejected":
        return "dot-rejected";

      default:
        break;
    }
  }
  /* ---------------------------- RENDERING HELPERS --------------------------- */
  function getHeaderCollapseMentorServiceCalendar(request) {
    const classname = getRequestClassName(request.status);
    return (
      <Row style={{ width: "100%" }}>
        <Col span={3}>{`${
          request.mentor?.firstName ? request.mentor?.firstName : "-"
        } ${request.mentor?.lastName ? request.mentor?.lastName : ""}`}</Col>

        <Col span={9} offset={2}>
          {request.title}
        </Col>
        <Col span={3} offset={1}>
          {
            mentorServices.find(
              (serv) => serv.identifier === request.serviceIdentifier
            )?.name
          }
        </Col>
        <Col span={2} style={{ right: "-1%" }}>
          {request.dates.length
            ? format(new Date(request.dates[0]), "dd/MM/yyy")
            : "-"}
        </Col>
        <Col span={4} style={{ right: "-2%" }}>
          <Row align="center">
            <Col>{Request.Time}</Col>&nbsp;
            <Col>
              <span class={classname}></span>
            </Col>
          </Row>
        </Col>
      </Row>
    );
  }

  function getCalendarRequest(request) {
    return (
      <Row className="collapse-wrapper-user-dash">
        <Collapse bordered={false}>
          <Panel header={getHeaderCollapseMentorServiceCalendar(request)}>
            <Space style={{ display: "flex" }} align="baseline">
              <div>
                <Row>
                  <Col span={9} offset={6}>
                    {request.subject}
                  </Col>
                  <Col
                    justify="end"
                    className="button-container-calendar-request"
                    span={7}
                    offset={2}
                  >
                    <Button className="request-scuedual-button" type="primary">
                      Request scuedual
                    </Button>
                    <Button className="request-scuedual-button" type="primary">
                      Important
                    </Button>
                    <Button className="request-scuedual-button" type="primary">
                      Reminder
                    </Button>
                  </Col>
                </Row>
              </div>
              <br></br>
              <div></div>
            </Space>
          </Panel>
        </Collapse>
      </Row>
    );
  }
  /* -------------------------------- RENDERING ------------------------------- */
  return (
    <div>
      <Row>
        <Col span={4} offset={2}>
          Mentor Name
        </Col>
        <Col span={9}>Subject</Col>
        <Col span={2}>Service</Col>
        <Col span={3} offset={1}>
          Date
        </Col>
        <Col span={2}>Time</Col>
      </Row>
      {mentorRequests.map((request) => getCalendarRequest(request))}
    </div>
  );
}

export default ScheduledSessions;
