/* -------------------------------------------------------------------------- */
/*                                Dependecies                                */
/* -------------------------------------------------------------------------- */

import React, { useState } from "react";
import { Button, Col, InputNumber, Layout, message, Row, Select } from "antd";
import { Check } from "tabler-icons-react";

//styles
import "./index.less";
import { useDispatch, useSelector } from "react-redux";
import {
  $updateMentorServices,
  selectSessionUser,
} from "../../../Session/slice";

// scoped componenets

const { Content } = Layout;

/* -------------------------------------------------------------------------- */
/*                                  Component                                 */
/* -------------------------------------------------------------------------- */
function Services() {
  const dispatch = useDispatch();
  /* ---------------------------- local variables  ---------------------------- */
  const priceLevels = ["0 - 49.99", "50 - 99.99", "100 - 199.99", "200 <"];
  const currentUser = useSelector(selectSessionUser);
  const [services, setServices] = useState(currentUser?.mentorServices);
  /* -------------------------------- CALLBACKS ------------------------------- */

  function handleCheck(serv) {
    const newServices = services.map((element) => {
      if (element.identifier === serv.identifier) {
        return { ...element, enabled: !element.enabled };
      } else return element;
    });

    setServices(newServices);
  }

  function handleChange(value, fieldName, service) {
    const newServices = services.map((element) => {
      if (element.identifier === service.identifier) {
        return { ...element, [fieldName]: value };
      } else return element;
    });

    setServices(newServices);
  }

  function onSaveNewServices() {
    dispatch(
      $updateMentorServices({ userId: currentUser?._id, services })
    ).then((action) => {
      if (action.error) {
        message.error("login error");
      } else {
        message.success("services updated in successfully");
      }
    });
  }
  /* ---------------------------- RENDERIN HELPERS ---------------------------- */

  function getMentorServices(service, index) {
    return (
      <Row className="service-row">
        <Col
          className={`service-check ${index === 0 ? "free" : ""}`}
          span={2}
          onClick={() => {
            handleCheck(service);
          }}
        >
          <Row justify="center" align="middle">
            {service.enabled ? <Check /> : null}
          </Row>
        </Col>
        <Col
          className={`service-col label ${index === 0 ? "free" : ""}`}
          span={10}
          offset={1}
        >
          {service.name}
        </Col>

        {service.identifier !== "visual-coffee" ? (
          <Col span={4} offset={1}>
            <Select
              defaultValue={service.priceRange}
              placeholder="Price range"
              options={priceLevels?.map((level) => ({
                label: level,
                value: level,
              }))}
              onChange={(e) => {
                handleChange(e, "priceRange", service);
              }}
            />
          </Col>
        ) : (
          <Col
            className={`service-col ${index === 0 ? "free" : ""}`}
            span={4}
            offset={1}
          >
            <Row justify="center" align="middle">
              {service.priceLevels}
            </Row>
          </Col>
        )}
        {service.identifier !== "visual-coffee" && (
          <Col span={4} offset={1}>
            <InputNumber
              defaultValue={service.price}
              placeholder="Price"
              onChange={(e) => {
                handleChange(e, "price", service);
              }}
            />
          </Col>
        )}
      </Row>
    );
  }
  /* ------------------------------- RENDERIN  ------------------------------- */

  return (
    <Content className="mentor-services-option">
      {services?.map((service, index) => getMentorServices(service, index))}
      <Row justify="end">
        <Col>
          <Button
            type="primary"
            className="save-changes-button"
            onClick={() => {
              onSaveNewServices();
            }}
          >
            Save changes
          </Button>
        </Col>
      </Row>
    </Content>
  );
}
export default Services;
