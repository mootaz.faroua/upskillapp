/* -------------------------------------------------------------------------- */
/*                                Dependecies                                */
/* -------------------------------------------------------------------------- */

import React, { useEffect, useState } from "react";
import { Row, Col, Collapse, Button, Layout, message } from "antd";
import { format } from "date-fns";
//styles
import "./index.less";
import axios from "axios";
import { baseURL } from "../../../../constants";
import { useSelector } from "react-redux";
import { selectSessionUser } from "../../../Session/slice";

// scoped componenets
const { Content } = Layout;
const { Panel } = Collapse;
/* -------------------------------------------------------------------------- */
/*                                  Component                                 */
/* -------------------------------------------------------------------------- */

function MentorRequests() {
  /* --------------------------------- HOOKS --------------------------------- */
  // redux
  const currentUser = useSelector(selectSessionUser);

  // states
  const [mentorRequests, setMentorRequests] = useState([]);

  // side- effects
  useEffect(() => {
    (async function () {
      try {
        await axios({
          method: "get",
          baseURL: `${baseURL}/mentoringRequest/getMentoringRequests`,
          params: { mentorId: currentUser._id },
        })
          .then((res) => {
            setMentorRequests(res.data);
          })
          .catch();
      } catch (e) {
        console.error(e);
      }
    })();
  }, []);
  /* -------------------------------- CALLBACKS ------------------------------- */

  async function onChangeStatus(status, request) {
    await axios({
      method: "post",
      baseURL: `${baseURL}/mentoringRequest/updateMentoringRequestStatus`,
      data: { status, mentoringRequestId: request._id },
    })
      .then((res) => {
        if (status === "accepted") {
          message.success("Reques accepted!");
        } else {
          message.success("Reques rejected!");
        }
        const newRequestsList = mentorRequests.map((req) =>
          req._id !== request._id ? req : res.data
        );
        setMentorRequests(newRequestsList);
      })
      .catch();
  }
  /* ---------------------------- RENDERING HELPERS --------------------------- */

  function getRequestClassName(status) {
    switch (status) {
      case "accepted":
        return "dot-accepted";
      case "pending":
        return "dot-pending";
      case "rejected":
        return "dot-rejected";

      default:
        break;
    }
  }

  function HeaderCollapse(request) {
    const classname = getRequestClassName(request.tatus);
    return (
      <Row style={{ width: "100%" }}>
        <Col span={4}>{`${
          request.sender?.firstName ? request.sender?.firstName : "-"
        } ${request.sender?.lastName ? request.sender?.lastName : ""}`}</Col>
        <Col span={12} offset={1}>
          {request.title}
        </Col>
        <Col span={3} offset={1}>
          {request.dates.length
            ? format(new Date(request.dates[0]), "dd/MM/yyy")
            : "-"}
        </Col>
        <Col span={3} style={{ right: "-2%" }}>
          <Row align="center">
            <Col>{request.status}</Col>&nbsp;
            <Col>
              <span class={classname}></span>
            </Col>
          </Row>
        </Col>
      </Row>
    );
  }

  function getRequestRow(request) {
    return (
      <Row className="collapse-wrapper-user-dash">
        <Collapse bordered={false}>
          <Panel header={HeaderCollapse(request)}>
            <div className="request-row">
              <Row>
                <Col span={12} offset={6}>
                  {request.subject}
                </Col>
              </Row>

              <Row justify="end">
                <Col style={{ width: "35%" }}>
                  <Row>
                    <Button
                      className="accept-button"
                      type="primary"
                      onClick={() => {
                        onChangeStatus("accepted", request);
                      }}
                    >
                      Accept
                    </Button>
                    &nbsp; &nbsp;
                    <Button
                      className="reject-button"
                      type="primary"
                      onClick={() => {
                        onChangeStatus("rejected", request);
                      }}
                    >
                      Reject
                    </Button>
                  </Row>
                </Col>
              </Row>
            </div>
          </Panel>
        </Collapse>
      </Row>
    );
  }
  /* -------------------------------- RENDERING ------------------------------- */
  return (
    <Content className="mentor-requests-content">
      <Row>
        <Col span={4} offset={2}>
          User
        </Col>
        <Col span={11}>Subject</Col>
        <Col span={3} offset={1}>
          Date
        </Col>
        <Col span={2}>Status</Col>
      </Row>
      {mentorRequests?.map((request) => getRequestRow(request))}
    </Content>
  );
}

export default MentorRequests;
