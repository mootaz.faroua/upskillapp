/* -------------------------------------------------------------------------- */
/*                                Dependecies                                */
/* -------------------------------------------------------------------------- */
import React, { useState, useEffect } from "react";
import { Line } from "@ant-design/plots";
//styles
import "./index.less";

/* -------------------------------------------------------------------------- */
/*                                  Component                                 */
/* -------------------------------------------------------------------------- */
function Graph({ color }) {
  const [data, setData] = useState([]);

  useEffect(() => {
    setData([
      {
        Date: "2012-03",
        scales: 0,
      },
      {
        Date: "2012-04",
        scales: 0,
      },
      {
        Date: "2012-05",
        scales: 0,
      },
      {
        Date: "2012-06",
        scales: 0,
      },
      {
        Date: "2012-07",
        scales: 0,
      },
      {
        Date: "2012-08",
        scales: 0,
      },
      {
        Date: "2012-09",
        scales: 500,
      },
      {
        Date: "2012-10",
        scales: 1000,
      },
      {
        Date: "2012-11",
        scales: 1201,
      },
      {
        Date: "2012-12",
        scales: 1065,
      },
      {
        Date: "2013-01",
        scales: 1100,
      },
      {
        Date: "2013-02",
        scales: 1200,
      },
      {
        Date: "2013-03",
        scales: 1398,
      },
      {
        Date: "2013-04",
        scales: 1678,
      },
      {
        Date: "2013-05",
        scales: 1600,
      },
      {
        Date: "2013-06",
        scales: 1688,
      },
      {
        Date: "2013-07",
        scales: 1700,
      },
      {
        Date: "2013-08",
        scales: 1670,
      },
      {
        Date: "2013-09",
        scales: 1734,
      },
      {
        Date: "2013-10",
        scales: 1800,
      },
      {
        Date: "2013-11",
        scales: 1508,
      },
      {
        Date: "2013-12",
        scales: 1480,
      },
      {
        Date: "2014-01",
        scales: 1350,
      },
      {
        Date: "2014-02",
        scales: 1202,
      },
      {
        Date: "2014-03",
        scales: 1334,
      },
      {
        Date: "2014-04",
        scales: 1723,
      },
      {
        Date: "2014-05",
        scales: 1330,
      },
      {
        Date: "2014-06",
        scales: 1280,
      },
      {
        Date: "2014-07",
        scales: 1367,
      },
      {
        Date: "2014-08",
        scales: 1155,
      },
      {
        Date: "2014-09",
        scales: 1289,
      },
      {
        Date: "2014-10",
        scales: 1204,
      },
      {
        Date: "2014-11",
        scales: 1246,
      },

      {
        Date: "2015-02",
        scales: 1276,
      },
      {
        Date: "2015-03",
        scales: 1033,
      },
      {
        Date: "2015-04",
        scales: 1000,
      },
      {
        Date: "2015-05",
        scales: 1000,
      },
      {
        Date: "2015-06",
        scales: 1089,
      },
      {
        Date: "2015-07",
        scales: 1000,
      },
      {
        Date: "2015-08",
        scales: 1043,
      },
      {
        Date: "2015-09",
        scales: 1000,
      },
      {
        Date: "2015-10",
        scales: 840,
      },
      {
        Date: "2015-11",
        scales: 934,
      },
      {
        Date: "2015-12",
        scales: 810,
      },
      {
        Date: "2016-01",
        scales: 782,
      },

      {
        Date: "2016-03",
        scales: 745,
      },
      {
        Date: "2016-04",
        scales: 680,
      },

      {
        Date: "2016-06",
        scales: 697,
      },
      {
        Date: "2016-07",
        scales: 583,
      },
      {
        Date: "2016-08",
        scales: 456,
      },
      {
        Date: "2016-09",
        scales: 524,
      },
      {
        Date: "2016-10",
        scales: 398,
      },
      {
        Date: "2016-11",
        scales: 278,
      },
    ]);
  }, []);

  const config = {
    data,
    padding: "auto",
    xField: "Date",
    yField: "scales",
    xAxis: {
      // type: 'timeCat',
      tickCount: 5,
    },
    smooth: true,
  };
  /* ---------------------------- Rendering Helpers ---------------------------- */

  /* -------------------------------- CALLBACKS ------------------------------- */

  /* ---------------------------- RENDERIN HELPERS ---------------------------- */

  return <Line color={color} {...config} />;
}
export default Graph;
