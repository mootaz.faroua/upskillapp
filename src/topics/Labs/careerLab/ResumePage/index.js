/* -------------------------------------------------------------------------- */
/*                                Dependencies                                */
/* -------------------------------------------------------------------------- */

// Packages
import React from "react";
import { format } from "date-fns";
// UI lib components
import { Avatar, Col, Layout, Progress, Row } from "antd";

// style
import "./index.less";
import { useSelector } from "react-redux";
import { selectSessionUser, selectUserCV } from "../../../Session/slice";

import FOULAN from "../../../../assets/mentorLab/foulan.png";
// scoped components
const { Content } = Layout;
function ResumePage() {
  console.log("in resule")
  /* ---------------------------------- HOOKS --------------------------------- */
  const currentUser = useSelector(selectSessionUser);
  const CurrentUserCV = useSelector(selectUserCV);
  /* ------------------------------ renderHelpers ----------------------------- */

  /* -------------------------------- RENDERING ------------------------------- */
  return (
    <Content className="resume-page">
      <Row className="resume">
        <Col span={8} className="left-column-container">
          <Row justify="center">
            <Col span={24} className="left-column-content">
              <Row>
                <div className="avatar-container">
                  <Avatar src={FOULAN} size={300} />
                </div>
              </Row>

              <Row className="row">ABOUT</Row>
              <Row>
                <Col>{CurrentUserCV.aboutMe}</Col>
              </Row>
              <Row className="row">CONTACT & INFO</Row>
             {currentUser.phone && <Row>{ `Phone: ${currentUser.phone}`}</Row>}
              <Row>Email: {currentUser.email}</Row>
              <Row>Address: {currentUser.address}</Row>
              <Row>
                Drive licence:
                {currentUser.DriveLicence && currentUser.DriveLicence === "yes"
                  ? "Yes"
                  : "No"}
              </Row>
              <Row>Nationality: {currentUser.nationalities?.join(" ;")}</Row>
              {currentUser.birthDate && <Row>
                Birth date:
                {format(new Date(currentUser.birthDate), "dd/MM/yyyy")}
              </Row>}

              <Row className="row">INTERESTS</Row>
              <Row>{CurrentUserCV.hobbies?.join(`  `)}</Row>
            </Col>
          </Row>
        </Col>
        <Col span={16} className="right-column-container">
          <Row className="right-column-content">
            <Col span={24}>
              <Row className="user-name-row">
                <Col className="firstName">
                  {currentUser?.firstName?.toUpperCase()}
                </Col>

                <Col className="lastName">
                  {currentUser?.lastName?.toUpperCase()}
                </Col>
              </Row>
              <Row className="user-work">{currentUser.currentWork}</Row>

              <Row className="second-section-row">
                <Col span={12} className="skills-row">
                  <div className="second-section-wrapper">
                    <Row className="skills-title">SKILLS</Row>
                    {CurrentUserCV.skills.map((skill, index) => (
                      <Row>
                        <Col span={8}>{skill}</Col>
                        <Col span={10} offset={2}>
                          <Progress
                            percent={(index + 2) * 10}
                            showInfo={false}
                            strokeColor={"#025bb4"}
                          />
                        </Col>
                      </Row>
                    ))}
                  </div>
                </Col>
                <Col span={8} offset={1} className="links-languages-row">
                  <Row className="title">LINKS</Row>
                  {["facebook", "instagram", "linkedin", "whatsapp"].map(
                    (link) => (
                      <Row>
                        <Col span={9}>{link}</Col>
                        <Col span={12} offset={1}>
                          {CurrentUserCV?.[`${link}Link`]}
                        </Col>
                      </Row>
                    )
                  )}
                  <Row className="title">LANGUAGES</Row>

                  <Row>{CurrentUserCV.languages?.join(" ")}</Row>
                </Col>
              </Row>
              <Row className="experience-row">
                <Col span={24}>
                  <Row className="title">EXPERIENCE</Row>

                  {CurrentUserCV.work?.map((work) => (
                    <Row className="work-row">
                      <Col span={4}>
                        {work.startDate && work.endDate && <Row>{`${format(
                          new Date(work.startDate),
                          "MM/yyyy"
                        )} - ${format(
                          new Date(work.endDate),
                          "MM/yyyy"
                        )}`}</Row>}
                        <Row>{work.location}</Row>
                      </Col>
                      <Col span={16} offset={2}>
                        <Row className="company">{work.company}</Row>
                        <Row className="job-title">{work.title}</Row>
                        <Row className="description">{work.mainTasks}</Row>
                        <Row></Row>
                      </Col>
                    </Row>
                  ))}
                </Col>
              </Row>
              <Row className="experience-row">
                <Col span={24}>
                  <Row className="title">INTERNSHIPS</Row>

                  {CurrentUserCV.internships?.map((internships) => (
                    <Row className="work-row">
                      <Col span={4}>
                        {internships.startDate && internships.endDate && <Row>{`${format(
                          new Date(internships.startDate),
                          "MM/yyyy"
                        )} - ${format(
                          new Date(internships.endDate),
                          "MM/yyyy"
                        )}`}</Row>}
                        <Row>{internships.location}</Row>
                      </Col>
                      <Col span={16} offset={2}>
                        <Row className="company">{internships.company}</Row>
                        <Row className="job-title">{internships.title}</Row>
                        <Row className="description">{internships.mainTasks}</Row>
                        <Row></Row>
                      </Col>
                    </Row>
                  ))}
                </Col>
              </Row>

              <Row className="education-row">
              <Col span={12}>
                  <Row className="title">EDUCATION</Row>
                  {CurrentUserCV.diplomas?.map((diplomas) => (
                    <Row className="work-row">
                      <Col span={6}>
                        <Row className="work-name">{diplomas.name}</Row>
                      </Col>
                      <Col span={14} offset={2}>
                        <Row className="company">
                          {format(
                          new Date(diplomas.date),
                          "yyyy"
                        )} &emsp; <div className="location">{diplomas.location}</div></Row>
                        <Row className="job-title">{diplomas.school}</Row>
                        <Row className="description">Mention : {diplomas.mention}</Row>
                        <Row></Row>
                      </Col>
                    </Row>
                  ))}
                </Col>
                <Col className="training-col" span={12}>
                  <Row className="title">TRAINING</Row>

                  {CurrentUserCV.trainings?.map((trainings) => (
                    <Row className="work-row">
                    <Col span={6}>
                      <Row className="work-name">{trainings.name}</Row>
                    </Col>
                    <Col span={14} offset={2}>
                      <Row className="company">
                       2022  &emsp;  <div className="location"> { trainings.periode } months</div></Row>
                      <Row className="job-title">{trainings.company}</Row>
                      <Row className="description">Link : {trainings.Link}</Row>
                      <Row></Row>
                    </Col>
                  </Row>
                  ))}
                </Col>
              </Row>
            </Col>
          </Row>
        </Col>
      </Row>
    </Content>
  );
}
export default ResumePage;
