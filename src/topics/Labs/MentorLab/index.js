/* -------------------------------------------------------------------------- */
/*                                Dependencies                                */
/* -------------------------------------------------------------------------- */

// Packages
import React, { useEffect, useState } from "react";

import Slider from "react-slick";

// UI lib components
import { Button, Col, Image, Layout, Rate, Row, Select } from "antd";
import Footer from "../../layouts/HomePage/Footer";
// Local ui components
import TopBar from "../../layouts/HomePage/topBar";

// style
import "./index.less";

//assests
import FOULAN from "../../../assets/mentorLab/foulan.png";
import {
  Award,
  Briefcase,
  Businessplan,
  CalendarMinus,
} from "tabler-icons-react";
import { HeartFilled } from "@ant-design/icons";
import axios from "axios";
import { baseURL } from "../../../constants";
import { useNavigate } from "react-router-dom";
import { useSelector } from "react-redux";
import { selectSessionUser } from "../../Session/slice";
// scoped components
const { Content } = Layout;
function CareerLabPage() {
  const navigate = useNavigate();
  const [mentors, setMentors] = useState([]);
  const currentUser = useSelector(selectSessionUser);

  useEffect(() => {
    (async function () {
      try {
        await axios({
          method: "get",
          baseURL: `${baseURL}/user/getUsersByRole`,

          params: { role: "mentor" },
        })
          .then((res) => {
            const Mentors = res.data?.filter(
              (mentorDoc) => mentorDoc._id !== currentUser._id
            );

            setMentors(Mentors);
          })
          .catch();
      } catch (e) {
        console.error(e);
      }
    })();
  }, []);

  /* ---------------------------------- HOOKS --------------------------------- */

  /* ------------------------------ renderHelpers ----------------------------- */

  function getStartedSection() {
    var settings = {
      dots: true,
      infinite: true,
      speed: 500,
      slidesToShow: 1,
      slidesToScroll: 1,
      fade: true,
      autoplaySpeed: 2000,
      draggable: false,
      cssEase: "linear",
      raggable: true,
      autoplay: true,
    };
    return (
      <Slider {...settings}>
        <div className="slide-mentorLab">
          <Row align="middle">
            <Col span={12} offset={6}>
              <Row justify="center" style={{ marginBottom: "-6%" }}>
                <h1 className="slider-subtitre">
                  <p className="p1">Mentors Lab</p>
                </h1>
              </Row>
              <Row justify="center" style={{ marginBottom: "-9%" }}>
                <h2 className="slider-subtitre">
                  <p className="p2">Find you match now!</p>
                </h2>
              </Row>
              <Row justify="center">
                <Button
                  style={{
                    width: "39%",
                    borderRadius: "50px",
                    color: "white",
                    backgroundColor: "#1034a8",
                    fontSize: "15px",
                  }}
                >
                  <label> Start the journey</label>
                </Button>
              </Row>
            </Col>
          </Row>
        </div>
      </Slider>
    );
  }

  function getMentorsList() {
    return mentors.map((mentor) => (
      <Col className="mentor-column" span={5} offset={1}>
        <Row className="image-wrapper">
          <Image preview={false} src={FOULAN}></Image>
          <Col
            span={24}
            className="conatiner-on-image"
            style={{ height: "100%" }}
          >
            <Row className="heart-row" justify="end">
              <HeartFilled size={50} />
            </Row>
            <Row className="row-container" align="bottom">
              <Col span={24}>
                <Row>
                  <Rate defaultValue={3}></Rate>
                </Row>
                <Row className="role">main skill</Row>
                <Row className="name">{`${mentor.firstName} ${mentor.lastName}`}</Row>
              </Col>
            </Row>
          </Col>
        </Row>
        <Row className="mentor-details">
          <Col>
            <Row>
              <Col>
                <Briefcase size={17} />
              </Col>
              <Col>Current job title</Col>
            </Row>
            <Row>
              <Col>
                <CalendarMinus size={17} />
              </Col>
              <Col>Years of experience</Col>
            </Row>
            <Row>
              <Col>
                <Award size={17} />
              </Col>
              <Col>Awards</Col>
            </Row>
            <Row>
              <Col>
                <Businessplan size={17} />
              </Col>
              <Col>$$$$$$$$$$$</Col>
            </Row>
          </Col>
        </Row>
        <Row justify="center">
          <Button
            className="mentor-profile-button"
            onClick={() => {
              navigate(`/mentorProfil/${mentor._id}`);
            }}
          >
            Profile
          </Button>
        </Row>
      </Col>
    ));
  }

  /* -------------------------------- RENDERING ------------------------------- */
  return (
    <Content className="mentor-lab-page">
      <TopBar />
      <Content className="mentor-lab-content">
        {getStartedSection()}
        <Row justify="center" className="row-wrapper">
          <Col span={24}>
            <Row className="filters-row">
              <Col span={24}>
                <Row className="first-row">
                  <Col span={4}>
                    <Select placeholder="Search by keyword" />
                  </Col>
                  <Col span={4} offset={1}>
                    <Select placeholder="Services offred" />
                  </Col>
                  <Col span={4} offset={1}>
                    <Select placeholder="Years of experience" />
                  </Col>
                  <Col span={4} offset={1}>
                    <Select placeholder="Industry" />
                  </Col>
                  <Col offset={1}>
                    <Button className="filter-button">Help</Button>
                  </Col>
                </Row>
                <Row>
                  <Col span={4}>
                    <Select placeholder="Rating" />
                  </Col>
                  <Col span={4} offset={1}>
                    <Select placeholder="Country" />
                  </Col>
                  <Col span={4} offset={1}>
                    <Select placeholder="Price" />
                  </Col>
                  <Col span={4} offset={1}>
                    <Select placeholder="Language" />
                  </Col>
                  <Col offset={1}>
                    <Button className="filter-button search-button">
                      Search
                    </Button>
                  </Col>
                </Row>
              </Col>
            </Row>
          </Col>
        </Row>
        <Row className="mentors-row"> {getMentorsList()}</Row>
        <Footer />
      </Content>
    </Content>
  );
}
export default CareerLabPage;
