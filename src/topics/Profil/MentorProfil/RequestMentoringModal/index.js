/* -------------------------------------------------------------------------- */
/*                                Dependecies                                */
/* -------------------------------------------------------------------------- */
import React from "react";
import { Layout, Form, Input, Modal, Row, message, InputNumber } from "antd";
import axios from "axios";

// local UI components
import DatePicker from "../../../Shared/components/inputs/DatePicker";
import TimePicker from "../../../Shared/components/inputs/TimePicker";

// Constants
import { baseURL } from "../../../../constants";

// style
import "./index.less";

// scoped componenets
const { Content } = Layout;
const { Item } = Form;
const { TextArea } = Input;

/* -------------------------------------------------------------------------- */
/*                                  Component                                 */
/* -------------------------------------------------------------------------- */
function RequestMentoringModal({
  isModalVisible,
  currentUserId,
  mentorId,
  setIsModalVisible,
  service,
}) {
  const [form] = Form.useForm();

  const formItemLayout = {
    labelCol: {
      span: 24,
    },
    wrapperCol: {
      span: 24,
    },
  };
  /* -------------------------------- CALLBACKS ------------------------------- */
  /* -------------------------------- CALLBACKS ------------------------------- */

  async function onSendMentoringRequest() {
    const formValues = form.getFieldsValue();

    const request = {
      senderId: currentUserId,
      mentorId,
      title: formValues.title,
      subject: formValues.subject,
      serviceIdentifier: service.identifier,
      date: formValues.date,
      price: service.price,
      time: formValues.time,
      period: formValues.period,
    };

    await axios({
      method: "post",
      baseURL: `${baseURL}/mentoringRequest/addMentoringRequest`,
      data: { request },
    })
      .then((res) => {
        message.success("Request sent!");
        setIsModalVisible(false);
      })
      .catch();
  }

  /* ---------------------------- Rendering Helpers ---------------------------- */
  function getRequestMentoringForm() {
    return (
      <Form form={form} {...formItemLayout}>
        <Row>
          <Item name="title" label="Title">
            <Input />
          </Item>
        </Row>
        <Row>
          <Item name="subject" label="Subject">
            <TextArea />
          </Item>
        </Row>
        <Row>
          <Item name="date" label="Date">
            <DatePicker />
          </Item>
        </Row>
        {service?.identifier === "development-journey" && (
          <Row>
            <Item name="period" label="period">
              <InputNumber />
            </Item>
          </Row>
        )}
        <Row>
          <Item name="time" label="Time">
            <TimePicker />
          </Item>
        </Row>
      </Form>
    );
  }

  return (
    <Content className="request-mentor-service">
      <Modal
        visible={isModalVisible}
        onCancel={() => {
          setIsModalVisible(false);
        }}
        title={`Request a ${service?.name}`}
        onOk={() => {
          onSendMentoringRequest();
        }}
      >
        <Content>{getRequestMentoringForm()}</Content>
      </Modal>
    </Content>
  );
}
export default RequestMentoringModal;
