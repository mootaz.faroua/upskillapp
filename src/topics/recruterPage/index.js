/* -------------------------------------------------------------------------- */
/*                                Dependecies                                */
/* -------------------------------------------------------------------------- */

import {
  Layout,
  Row,
  Form,
  Input,
  Button,
  Col,
  Card,
  Divider,
  Typography,
} from "antd";

import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { $addJobOffer, selectJobOffers } from "./slice";
// scoped componenets

const { Content } = Layout;
const { Item } = Form;
const { TextArea } = Input;
const { Title } = Typography;

/* -------------------------------------------------------------------------- */
/*                                  Component                                 */
/* -------------------------------------------------------------------------- */
function RecructerPage() {
  const [form] = Form.useForm();
  const dispatch = useDispatch();
  const jobOffers = useSelector(selectJobOffers);
  /* ---------------------------- Rendering Helpers ---------------------------- */

  /* -------------------------------- CALLBACKS ------------------------------- */
  function onAddJobOffer() {
    dispatch($addJobOffer(form.getFieldsValue())).then((action) => {});
  }
  /* ---------------------------- RENDERIN HELPERS ---------------------------- */

  function getJobOffers() {
    return jobOffers?.map((jobOffer) => (
      <Col offset={1}>
        <Card title={jobOffer.title} style={{ width: 300 }}>
          {jobOffer?.description}
        </Card>
      </Col>
    ));
  }

  function getAddJobOfferForm() {
    return (
      <Form form={form}>
        <Row>
          <Item name="title" label="Title">
            <Input></Input>
          </Item>
        </Row>
        <Row>
          <Item name="description" label="Description"></Item>
          <TextArea></TextArea>
        </Row>
        <Row>
          <Item>
            <Button
              onClick={() => {
                onAddJobOffer();
              }}
            >
              Add job offer
            </Button>
          </Item>
        </Row>
      </Form>
    );
  }
  return (
    <Content>
      <Row>
        <Col offset={2} span={20}>
          {getAddJobOfferForm()}
        </Col>
      </Row>
      <Divider></Divider>
      <Row justify="center">
        <Title level={2}> Job offers list</Title>
      </Row>
      <Row justify="center">{getJobOffers()}</Row>
    </Content>
  );
}
export default RecructerPage;
