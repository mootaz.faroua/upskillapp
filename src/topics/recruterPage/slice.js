import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";
import { baseURL } from "../../constants";

/**
 * Helper for the redundant initial state
 */
const getInitialState = () => ({
  jobOffersList: [],
});

/**
 * Creates a login session
 */
export const $addJobOffer = createAsyncThunk(
  "Session/addJobOffer",

  async (data) => {
    const request = {
      method: "post",
      url: `${baseURL}/jobOffer/addJobOffer`,
      data: data,
    };
    const payload = await axios(request);
    return payload.data;
  }
);

// Session Slice
const JobOffer = createSlice({
  name: "JobOffer",
  initialState: getInitialState(),
  reducers: {},
  extraReducers: {
    [$addJobOffer.fulfilled]: (state, action) => {
      const stateUpdate = { ...state };

      const userData = action.payload;

      stateUpdate.jobOffersList = userData;

      stateUpdate.loading = false;
      return stateUpdate;
    },
    [$addJobOffer.pending]: (state) => {
      const stateUpdate = { ...state };
      stateUpdate.loading = true;
      return stateUpdate;
    },
    [$addJobOffer.rejected]: (state) => {
      const stateUpdate = { ...state };
      stateUpdate.loading = false;
      return stateUpdate;
    },
  },
});

export default JobOffer.reducer;

// Simple actions
export const { $logout, forgotPassword, resetPassword, activateAccount } =
  JobOffer.actions;

// Selectors
export const selectJobOffers = (state) => state?.JobOffer?.jobOffersList;
