/* eslint-disable */
/* -------------------------------------------------------------------------- */
/*                                Dependencies                                */
/* -------------------------------------------------------------------------- */

// Packages
import React, { useState, useEffect, useRef } from "react";
import { useTranslation } from "react-i18next";

import PropTypes from "prop-types";
import { useDispatch, useSelector } from "react-redux";

import LoginModal from "../../Session/loginModal";
import SignInPage from "../../Session/signInPage";

// UI
import { Modal } from "antd";
// Redux
import { $setModalContext, selectModalContext } from "../slice";

// style
import "./index.less";

/* -------------------------------------------------------------------------- */
/*                                  Component                                 */
/* -------------------------------------------------------------------------- */

function ModalLayout({ isVisible }) {
  /* ********************************** CONSTANTS ********************************* */
  const modalContext = useSelector(selectModalContext);
  const FORM_WIDTH = 900;
  /* ********************************** HOOKS ********************************* */
  const dispatch = useDispatch();
  const modalContent = useSelector(selectModalContext);
  // Localization
  const { t } = useTranslation("Task");

  const ref = useRef();

  // State
  const [modalTitle, setModalTitle] = useState(undefined);
  const [formContent, setFormContent] = useState({});

  // Effects
  useEffect(() => {
    switch (modalContext) {
      case "login":
        setModalTitle(t("login"));
      case "Signup":
        setModalTitle(t("Signup"));

        break;
      case "modificationWorkSheetForm":
        setModalTitle("modification-sheet");
        break;
      default:
        setModalTitle(undefined);
    }
  }, [modalContext]);
  /* ********************************** CALLBACKS ********************************* */

  const onCancel = () => {
    dispatch($setModalContext(null));
    // ref.current.resetFields();
  };

  function getModalContent() {
    switch (modalContext) {
      case "login":
        return <LoginModal />;
      case "Signup":
        return <SignInPage />;

      default:
        return <></>;
    }
  }

  /* ******************************** RENDERING ******************************* */
  return (
    <Modal
      className="modal-wrapper"
      visible={isVisible}
      title=""
      width={FORM_WIDTH}
      okText={t("addTask.valider")}
      cancelText={t("addTask.annuler")}
      footer={null}
      onCancel={() => {
        onCancel();
      }}
    >
      {getModalContent()}
    </Modal>
  );
}
ModalLayout.propTypes = {
  isVisible: PropTypes.bool,
  modalContext: PropTypes.string,
};

ModalLayout.defaultProps = {
  isVisible: false,
  modalContext: undefined,
};
export default ModalLayout;
