// Import Swiper React components
import { Row, Col, Image,Button } from "antd";
import React from "react";

// assets
import StuckSec from "../../../../../assets/homePage/StuckSec.png";

// styles
import "./index.less";

function BimLab() {
  return (
    <div>
      <Row className="rowContainer1" justify="center" align="middle">
        <Col span={8} offset={4}  >
          <Row>
            <h5 className="slider-subtitre">
              {" "}
              <p>STUCK ? NEED</p>
              <p>AN EXPERT ?</p>{" "}
            </h5>
          </Row>
          <Row>
            <h2 className="slider-titre">
              {" "}
              <p className="p1">Solution is a meeting away </p>{" "}
            </h2>
          </Row>
          <Row>
            <div className="slider-para">
              {" "}
              <p>Our list of verified expertsis one click away.</p>
              <p>Explore our mentors and find your match now !</p>
            </div>
          </Row>

          <Row>
              <Button className="slider-button"  >
                <label> Find a match</label>
              </Button></Row>
        </Col>
        <Col span={10}>
          <Row className="row-image1">
            <Image preview={false} src={StuckSec}></Image>
          </Row>
        </Col>
      </Row>
    </div>
  );
}
export default BimLab;
