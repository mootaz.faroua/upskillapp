// Import Swiper React components
import { Row, Col, Image, Divider, Button } from "antd";
import React from "react";
import { IconChartBar } from "@tabler/icons";

// assets
import slider3 from "../../../../../assets/slider3.png";

// styles
import "./index.less";


function Community() {
  return (
    
    <div className="container-Community" align="middle" justify="space-around">
      <Row>
        <Col span={10} offset={2}>
          <Row>
            <h4 className="Community-subtitre">Une large sélection de cours</h4>
          </Row>
          <Row>
            <h3 className="container-title">Community</h3>
            </Row>
            <Row className="row-divider">
            <Divider></Divider>
          </Row>
          <Row>
            <div className="container-text">
              Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
              eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut
              enim ad minim veniam, quis nostrud exercitation ullamco laboris
              nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in
              reprehenderit in voluptate velit esse cillum dolore eu fugiat
              nulla pariatur
            </div>
          </Row>




<br></br>

          <Row className="Container-all">
            <div className="item1">
            <Col>
                <Divider
                  className="ant-divider-vertical-item"
                  type="vertical"
                  style={{ border: "1px solid #e07475",backgroundColor: "#e07475" }}
                ></Divider> 
                </Col>
                <Col> 
                 <IconChartBar className="icon-vertical" color={"#e07475"} style={{backgroundColor: "#f3e6e6"}}/>
                 </Col>
                 <Col className="text-item" >
                 <Row >
                <label >&nbsp;&nbsp;Testing</label>
                </Row>
                <Row>
                <label >&nbsp;&nbsp;2051</label>
                </Row>
                </Col>
             </div>
            <div className="item1"> <Col>
                <Divider
                  className="ant-divider-vertical-item"
                  type="vertical"
                  style={{ border: "1px solid #53444f",backgroundColor: "#53444f" }}
                ></Divider> 
                </Col>
                <Col> 
                 <IconChartBar className="icon-vertical" color={"#53444f"} style={{backgroundColor: "#dfe0e3"}} />
                 </Col>
                 <Col className="text-item">
                 <Row >
                <label >&nbsp;&nbsp;Working</label>
                </Row>
                <Row>
                <label >&nbsp;&nbsp;255</label>
                </Row>
                </Col>
                </div>
            <div className="item3"></div>
          </Row>

          <br></br>
          <Row className="Container-all">
            <div className="item1">
            <Col>
                <Divider
                  className="ant-divider-vertical-item"
                  type="vertical"
                  style={{ border: "1px solid #f59651",backgroundColor: "#f59651" }}
                ></Divider> 
                </Col>
                <Col> 
                 <IconChartBar className="icon-vertical" color={"#f59651"} style={{backgroundColor: "#f2ece6"}}/>
                 </Col>
                 <Col className="text-item" >
                 <Row >
                <label >&nbsp;&nbsp;Gaming</label>
                </Row>
                <Row>
                <label >&nbsp;&nbsp;2051</label>
                </Row>
                </Col>
             </div>
            <div className="item1"> <Col>
                <Divider
                  className="ant-divider-vertical-item"
                  type="vertical"
                  style={{ border: "1px solid #60ba69",backgroundColor: "#60ba69" }}
                ></Divider> 
                </Col>
                <Col> 
                 <IconChartBar className="icon-vertical" color={"#60ba69"} style={{backgroundColor: "#deeee4"}}/>
                 </Col>
                 <Col className="text-item">
                 <Row >
                <label >&nbsp;&nbsp;Coding</label>
                </Row>
                <Row>
                <label >&nbsp;&nbsp;255</label>
                </Row>
                </Col>
                </div>
            <div className="item3"></div>
          </Row>

          <Row>
            <Button className="row-button" type="primary" align="left">
              Get started
            </Button>
          </Row>
          
        </Col>
        <Col span={8} offset={4} className="test">
          <Image
            preview={false}
            src={slider3}
            className="image-container-Community"
          ></Image>
        </Col>
      </Row>
    </div>
  );
}
export default Community;
