/* -------------------------------------------------------------------------- */
/*                                Dependencies                                */
/* -------------------------------------------------------------------------- */
// packages
import React from "react";

// lib components
import { Row, Col, Image, Layout, Typography, Rate } from "antd";
// assets
import PROFILE1 from "../../../../../assets/homePage/profile1.png";
import PROFILE2 from "../../../../../assets/homePage/profile2.png";
import PROFILE3 from "../../../../../assets/homePage/profile3.jpeg";
import PROFILE4 from "../../../../../assets/homePage/profile4.jpg";
import CIRCLE from "../../../../../assets/homePage/circle.png";
import LINE from "../../../../../assets/homePage/line2.png";
import STAR from "../../../../../assets/homePage/star.png";

// styles
import "./index.less";

// scoped components
const { Content } = Layout;
const { Text } = Typography;
/* -------------------------------------------------------------------------- */
/*                                  Component                                 */
/* -------------------------------------------------------------------------- */
function MentorsSection() {
  /* -------------------------------- RENDERING ------------------------------- */
  return (
    <Content className="mentors-section-container">
      <Image className="image-circle" preview={false} src={CIRCLE}></Image>
      <Image className="image-line" preview={false} src={LINE}></Image>
      <Image className="image-line2" preview={false} src={LINE}></Image>
      <Image className="image-star" preview={false} src={STAR}></Image>
      <Image className="image-star2" preview={false} src={STAR}></Image>
      <Row justify="center" align="middle">
        <Text className="title1"> Our most popular</Text>
      </Row>
      <Row justify="center" align="middle">
        <Text className="title2">Mentors</Text>
      </Row>
      <Row justify="center" align="middle">
        <Text className="subtitle">
          Check our fine selection of our most popular
        </Text>
      </Row>
      <Row justify="center" align="middle">
        <Text className="subtitle">experts and find your match today !</Text>
      </Row>
      <Row className="containerGris" justify="center" align="top">
        <Col className="mentor-container" span={3}>
          <Row justify="center" align="middle">
            <Image className="image" preview={false} src={PROFILE1}></Image>
          </Row>
          <Row>
            <Rate value={5} className="rate-stars" />
          </Row>
          <Row>
            <Text className="mentor-name">Barbara Bates</Text>
          </Row>
          <Row>
            <Text className="mentor-job">PROFESSIONAL TRAINING & COACHING</Text>
          </Row>
          <Row>
            <Text className="mentor-expertise">Coach & writer</Text>
          </Row>
          <Row>
            <Text className="mentor-experience">20 + years experience </Text>
          </Row>
          <Row>
            <Text className="mentor-experience">Price -$$ </Text>
          </Row>
        </Col>

        <Col className="mentor-container" span={3}>
          <Row justify="center" align="middle">
            <Image className="image" preview={false} src={PROFILE2}></Image>
          </Row>
          <Row>
            <Rate value={5} className="rate-stars" />
          </Row>
          <Row>
            <Text className="mentor-name">Beccy Ivring</Text>
          </Row>
          <Row>
            <Text className="mentor-job">MARKETING & ADVERTISONG</Text>
          </Row>
          <Row>
            <Text className="mentor-expertise">Coach & writer</Text>
          </Row>
          <Row>
            <Text className="mentor-experience">20 + years experience </Text>
          </Row>
          <Row>
            <Text className="mentor-experience">Price -$$ </Text>
          </Row>
        </Col>

        <Col className="mentor-container" span={3}>
          <Row justify="center" align="middle">
            <Image className="image" preview={false} src={PROFILE3}></Image>
          </Row>
          <Row>
            <Rate value={5} className="rate-stars" />
          </Row>
          <Row>
            <Text className="mentor-name">Christelle Kerouedan</Text>
          </Row>
          <Row>
            <Text className="mentor-job">PROFESSIONAL TRAINING & COACHING</Text>
          </Row>
          <Row>
            <Text className="mentor-expertise">Coach & writer</Text>
          </Row>
          <Row>
            <Text className="mentor-experience">20 + years experience </Text>
          </Row>
          <Row>
            <Text className="mentor-experience">Price -$$ </Text>
          </Row>
        </Col>

        <Col className="mentor-container" span={3}>
          <Row justify="center" align="middle">
            <Image className="image" preview={false} src={PROFILE4}></Image>
          </Row>
          <Row>
            <Rate value={5} className="rate-stars" />
          </Row>
          <Row>
            <Text className="mentor-name">Derek Bates</Text>
          </Row>
          <Row>
            <Text className="mentor-job">FINANCIAL SERVICES</Text>
          </Row>
          <Row>
            <Text className="mentor-expertise">Coach & writer</Text>
          </Row>
          <Row>
            <Text className="mentor-experience">20 + years experience </Text>
          </Row>
          <Row>
            <Text className="mentor-experience">Price -$$ </Text>
          </Row>
        </Col>
      </Row>
    </Content>
  );
}
export default MentorsSection;
