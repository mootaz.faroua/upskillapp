// Import Swiper React components
import React from "react";
import Slider from "react-slick";
import { Image, Row, Col, Button } from "antd";

// assets
import slider1 from "../../../../../assets/slider1.png";
import Facebook from "../../../../../assets/Facebook.png";
import Messenger from "../../../../../assets/Messenger.png";
import Whatsapp from "../../../../../assets/Whatsapp.png";
import Insta from "../../../../../assets/Insta.png";
import linkedin from "../../../../../assets/linkedin.png";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";

import "./index.less";
// import { useTranslation } from "react-i18next";
function Slide() {
  // const { t } = useTranslation("layouts");
  var settings = {
    dots: true,
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
    fade: true,
    autoplaySpeed: 2000,
    draggable: false,
    cssEase: "linear",
    raggable: true,
    autoplay: true,
    backgroundImage: `url(${slider1})`,
    backgroundPosition: "center",
    backgroundSize: "cover",
    backgroundRepeat: "no-repeat",
  };
  return (
    <div>
      <Slider {...settings}>
        <div className="slide-test">
          <Row align="middle">
            <Col span={4} offset={3}>
              <Row>
                <h1 className="slider-subtitre">
                  <p className="p1">WORK</p> <p className="p1">SMART</p>{" "}
                </h1>
              </Row>
              <h2 className="slider-titre">
                <p className="p1">Not Hard </p>{" "}
              </h2>
              <h5 className="slider-text">Request a mentorship now !</h5>
              <br></br>
              <Button className="slider-button">
                <label> Schedule a meeting</label>
              </Button>
            </Col>
            <Col span={1} offset={16}>
              <div style={{ margin: "0% 0% 345% 0%" }}>
                <Image
                  preview={false}
                  src={linkedin}
                  style={{ width: "30%" }}
                ></Image>
                <Image
                  preview={false}
                  src={Facebook}
                  style={{ width: "30%" }}
                ></Image>
                <Image
                  preview={false}
                  src={Insta}
                  style={{ width: "30%" }}
                ></Image>
                <Image
                  preview={false}
                  src={Whatsapp}
                  style={{ width: "30%" }}
                ></Image>
                <Image
                  preview={false}
                  src={Messenger}
                  style={{ width: "30%" }}
                ></Image>
              </div>
            </Col>
          </Row>
        </div>
        <div className="slide2-test">
          <Row align="middle">
            <Col span={5} offset={3}>
              <Row>
                <h1 className="slider-subtitre">
                  <p className="p1">TIME TO</p> <p className="p1">SKILL UP</p>{" "}
                </h1>
              </Row>
              <h2 className="slider-titre">
                {" "}
                <p className="p1">The easy way </p>{" "}
              </h2>
              <h5 className="slider-text">Our course collection is clear,</h5>
              <h5 className="slider-text">easy and straight to the point.</h5>
              <h5 className="slider-text">Start learning now !</h5>
              <br></br>
              <Button className="slider-button">
                <label> Explore our labs</label>
              </Button>
            </Col>
            <Col span={1} offset={15}>
              <div style={{ margin: "0% 0% 345% 0%" }}>
                <Image
                  preview={false}
                  src={linkedin}
                  style={{ width: "30%" }}
                ></Image>
                <Image
                  preview={false}
                  src={Facebook}
                  style={{ width: "30%" }}
                ></Image>
                <Image
                  preview={false}
                  src={Insta}
                  style={{ width: "30%" }}
                ></Image>
                <Image
                  preview={false}
                  src={Whatsapp}
                  style={{ width: "30%" }}
                ></Image>
                <Image
                  preview={false}
                  src={Messenger}
                  style={{ width: "30%" }}
                ></Image>
              </div>
            </Col>
          </Row>
        </div>
      </Slider>
    </div>
  );
}
export default Slide;
