// Import Swiper React components
import { Row, Col, Image, Divider, Button, Layout} from "antd";
import React from "react";
// assets
import slider2 from "../../../../../assets/slider2.png";

// styles
import "./index.less";


const { Content } = Layout;

function needMentorship() {
  return (
    <Content className="" >

     
       <Row style={{  background: "linear-gradient(0deg,#efdec7,#fdfae6)"}}>
          <Col  span={10} offset={3} >
             <div className="container-Box">
       <div className="item"> Construction</div>
       <div className="item"> Industriel</div>
            </div>
            </Col>
          <Col span={10} >
          <div className="container-Box">
       <div className="item"> Marketing</div>
       <div className="item"> Creative</div>
       </div>
            </Col>
        </Row>
    

    <div className="container-needMentorship" align="middle" justify="space-around">
        <Row>
          <Col  span={10} offset={2}>
          <Image preview={false} src={slider2} className="image-container-needMentorship"></Image>
            </Col>
          <Col span={8} offset={4} className="test">
          <Row><h4 className="needMentorship-subtitre">An expert that can guide you</h4></Row>
          <Row>
          <h3 className="container-title">Need Mentorship</h3>
          </Row>
          <Row>
          <Divider ></Divider>
          </Row>
          <Row>
            <div className="container-text">
              Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
              eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut
              enim ad minim veniam, quis nostrud exercitation ullamco laboris
              nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in
              reprehenderit in voluptate velit esse cillum dolore eu fugiat
              nulla pariatur
            </div>
         </Row>
         <Row>
        <Button type="primary" align="left">Get started</Button>
        </Row>
          </Col>
        </Row>
    </div>
    </Content>
  );
}
export default needMentorship;
