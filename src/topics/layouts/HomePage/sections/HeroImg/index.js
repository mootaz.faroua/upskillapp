// Import Swiper React components
import { Row, Col, Image,Tooltip, Input } from "antd";
import React from "react";
// assets Img
import HommeHomePage from "../../../../../assets/homePage/HommeHomePage.png";
import femmeHomePage from "../../../../../assets/homePage/womanhero.png";
import vecHomePage from "../../../../../assets/homePage/vecHomePage.png";
import EtoileHomePage from "../../../../../assets/homePage/EtoileHomePage.png";

//Import css
import "./index.less";
import { SearchOutlined } from "@ant-design/icons";

function HeroImg() {
    const suffix = <SearchOutlined />;
    return (
    <Row className="rowContainer" justify="center">
    <Col span={8} offset={2} className="SectionGauche">
        <Row>
        <p className="textGauche">
            Access To <p className="textGauchehundreds">hundreds</p>
              Of experts in one click !
        </p>
        <Input className="input-text"   placeholder="What do you need ?"   suffix={suffix} />
        </Row>
    </Col>    
    <Col span={14} className="SectionDroite">    
    <Image className="EtoileHomePage" preview={false} src={EtoileHomePage}  ></Image>
    <Image className="HommeHomePage" preview={false} src={HommeHomePage}  ></Image>
    <Image className="FemmeHomePage" preview={false} src={femmeHomePage}  ></Image>
    <Image className="vecHomePage" preview={false} src={vecHomePage}  ></Image>
    </Col>   
    </Row>
    
    );
}
export default HeroImg;