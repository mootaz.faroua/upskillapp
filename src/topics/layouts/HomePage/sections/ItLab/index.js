/* -------------------------------------------------------------------------- */
/*                                 Dpendencies                                */
/* -------------------------------------------------------------------------- */
// Lib dependencies
import React from "react";

// UI LIB dependencies
import { Row, Col, Image, Button, Typography } from "antd";
// assets
import PC from "../../../../../assets/homePage/Pc.jpeg";

// styles
import "./index.less";

// Scoped components
const { Text } = Typography;
/* -------------------------------------------------------------------------- */
/*                                  Component                                 */
/* -------------------------------------------------------------------------- */
function ItLab() {
  return (
    <div>
      <Row className="itlab-container" justify="center" align="middle">
        <Col span={10}>
          <Row justify="center">
            <Image className="image" preview={false} src={PC}></Image>
          </Row>
        </Col>
        <Col span={8}>
          <Row className="title-container" justify="start">
            <Text className="title">
              Is <span className="resume"> my resume</span> good enough ?
            </Text>
          </Row>

          <Row>
            <Col className="slider-para">
              <Row>
                Thanks to our advances processing, we will help you create your
                resume in a click. Stop worring about standing out.
              </Row>
            </Col>
          </Row>
          <Row>
            <Button className="slider-button">
              <Text className="button-text"> Upload my CV </Text>
            </Button>
          </Row>
        </Col>
      </Row>
    </div>
  );
}
export default ItLab;
