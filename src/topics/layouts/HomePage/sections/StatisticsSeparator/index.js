// Import Swiper React components

import { Row, Image, Button } from "antd";
import React from "react";

// assets
import SepUpImgFollow from "../../../../../assets/homePage/SepUpImgFollow.png";
// styles
import "./index.less";

function CarrierePlannification() {
  return (
    <div className="separator-container">
      <div justify="center" align="middle">
        <Row>
          <Image
            className="IMG-Follow"
            preview={false}
            src={SepUpImgFollow}
          ></Image>
        </Row>
        <Row justify="center" align="middle" className="follow-title">
          <p>FOLLOW US NOW</p>
        </Row>
        <Row justify="center" align="middle" className="follow-Subtitle">
          <p>Be the first to get the special offers</p>
        </Row>
        <Row>
          <Button className="slider-button">
            <label> Build my resume</label>
          </Button>
        </Row>
      </div>
    </div>
  );
}
export default CarrierePlannification;
