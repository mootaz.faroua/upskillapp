/* -------------------------------------------------------------------------- */
/*                                Dependencies                                */
/* -------------------------------------------------------------------------- */

// Lib dependencies
import React from "react";
// import { useNavigate } from "react-router-dom";

// UI lib components
import { Menu, Row, Avatar, Layout, Space } from "antd";
import { User as IconUser } from "tabler-icons-react";
import { CloseOutlined } from "@ant-design/icons";

// style
import "./index.less";
import { useNavigate } from "react-router-dom";

// Scoped components
const { Item, Divider } = Menu;
const { Content } = Layout;

/* -------------------------------------------------------------------------- */
/*                                  Component                                 */
/* -------------------------------------------------------------------------- */
function NotificationItem({ notification, isLast }) {
  /* ********************************** HOOKS ********************************* */
  const navigate = useNavigate();
  /* ***************************** LOCAL VARIABLES **************************** */
  // let routeState;

  const { topic, action } = notification;
  /* ******************************* CALLBACKS ******************************* */
  let routeState;
  // resolves the notification route
  function getNavigationRoute() {
    switch (topic) {
      case "mentoringRequest": {
        if (action === "submit") {
          return "/dashboard/services#requests";
        } else if (action === "accept") {
          return `/dashboard/myMentoringLab#scheduled-sessions`;
        } else if (action === "reject") {
          return "/dashboard/myMentoringLab#requests-history";
        }
      }
      default:
        break;
    }
  }
  /**
   * Navigate user to source event in which notification was fired
   */
  async function onClickNotification() {
    const navigationRoute = getNavigationRoute();
    navigate(navigationRoute, routeState);
  }
  // async function removeNotification() {
  //   await asyncMethodCall(deleteNotification, {
  //     notificationId: notification._id,
  //   });
  // }

  /**
   * Extract and compose a notification message to be rendered
   */
  function getNotificationContent() {
    switch (topic) {
      case "mentoringRequest": {
        if (action === "submit") {
          return "You have a new mentoring request";
        } else if (action === "accept") {
          return ` ${notification?.sender.firstName}  ${notification?.sender.lastName} accepted your  mentoring request`;
        } else if (action === "reject") {
          return `${notification?.sender.firstName}  ${notification?.sender.lastName} rejected your mentoring request`;
        }
        return null;
      }

      default:
        return null;
    }
  }
  /* ******************************** RENDERING ******************************* */
  return (
    <Content
      onClick={function cb() {
        onClickNotification();
      }}
    >
      <Item>
        <Row justify="space-between" align="middle">
          <Space>
            <Avatar
              className="avatar-icon-container"
              size="large"
              icon={<IconUser size={16} />}
            />

            {getNotificationContent()}

            <div
              onClick={function cb(e) {
                e.stopPropagation();
                // removeNotification();
              }}
            >
              <CloseOutlined />
            </div>
          </Space>
        </Row>
      </Item>
      {!isLast && <Divider></Divider>}
    </Content>
  );
}
export default NotificationItem;
