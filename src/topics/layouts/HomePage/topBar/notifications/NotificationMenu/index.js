/* -------------------------------------------------------------------------- */
/*                                Dependencies                                */
/* -------------------------------------------------------------------------- */

// Lib dependencies
import React, { useState, useEffect } from "react";
import axios from "axios";

// UI lib components
import { Dropdown, Menu, Badge, Layout } from "antd";
import { Bell as IconNotification } from "tabler-icons-react";

// UI local components
import NotificationItem from "../NotificationItem";

// constants
import { baseURL } from "../../../../../../constants";
// style
import "./index.less";
import { useSelector } from "react-redux";
import { selectSessionUser } from "../../../../../Session/slice";

// Scoped components
const { Content } = Layout;
const { Item } = Menu;
/* -------------------------------------------------------------------------- */
/*                                  Component                                 */
/* -------------------------------------------------------------------------- */

function NotificationMenu() {
  /* ******************************** HOOKS ******************************* */
  const [notifications, setNotifications] = useState([]);
  const currentUser = useSelector(selectSessionUser);
  // side- effects
  useEffect(() => {
    axios({
      method: "get",
      baseURL: `${baseURL}/notification/getUserNotifications`,
      params: { userId: currentUser._id },
    })
      .then((res) => {
        setNotifications(res.data);
      })
      .catch();
  }, [currentUser]);
  /* ---------------------------- RENDERING HELPERS --------------------------- */
  function getNotificationsMenu() {
    return (
      <Menu>
        {notifications?.map((notif, index) => (
          <NotificationItem
            notification={notif}
            isLast={index === notifications?.length - 1}
          />
        ))}
        {notifications.length === 0 && <Item>No Notifications </Item>}
      </Menu>
    );
  }

  // // Mark notifications as read when opening notification menu
  // async function markNotificationsAsRead() {
  //   await asyncMethodCall(setAsRead, {
  //     notificationList: notifications?.map((notif) => notif._id),
  //   });
  //   return;
  // }

  /* ******************************** RENDERING ******************************* */
  return (
    <Content className="notification-container">
      <Dropdown
        overlay={getNotificationsMenu()}
        trigger={["click"]}
        placement="bottom"
        // onVisibleChange={(e) => {
        //   if (!e) {
        //     markNotificationsAsRead();
        //   }
        // }}
      >
        {/* <div type="primary" className="notification-button"> */}
        <Badge count={notifications?.length} offset={[-24, 0]}>
          <IconNotification className="action-icon" size={16} />
        </Badge>
        {/* </div> */}
      </Dropdown>
    </Content>
  );
}
export default NotificationMenu;
