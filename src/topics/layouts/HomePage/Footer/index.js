/* -------------------------------------------------------------------------- */
/*                                DEpendencies                                */
/* -------------------------------------------------------------------------- */
// Lib packages
import React from "react";

//UI lib components
import { Layout, Image, Space, Row, Col, Typography, Divider } from "antd";
import {
  IconBrandFacebook,
  IconBrandYoutube,
  IconBrandTwitter,
  IconBrandInstagram,
} from "@tabler/icons";
// Assets
import logo from "../../../../assets/logo.png";

// styles
import "./index.less";

// scoped compoenets
const { Text } = Typography;
/* -------------------------------------------------------------------------- */
/*                                  Component                                 */
/* -------------------------------------------------------------------------- */
function Footer() {
  // Localization
  const { Content } = Layout;

  return (
    <Content className="footer-container">
      <Row className="container1">
        <Col span={5} className="col1">
          <Row>
            <Image
              className="item-4-img"
              width="100px"
              preview={false}
              src={logo}
            ></Image>
          </Row>
          <Row>Discover the best version of</Row>
          <Row>yourself with Coaching &</Row>
          <Row>Mentoring on-demand</Row>
        </Col>
        <Col span={4} offset={1}>
          <Row>Home</Row>
          <Row>Training Lab</Row>
          <Row>Menotr Lab</Row>
          <Row>Career Lab</Row>
        </Col>
        <Col span={4} offset={1}>
          <Row>Become Mentor</Row>
          <Row>Become Instructor</Row>
          <Row>For Business</Row>
          <Row>For Partnership</Row>
        </Col>
        <Col span={4} offset={1}>
          <Row>FAQ</Row>
          <Row>News</Row>
          <Row>Contact</Row>
          <Row>Join Us</Row>
        </Col>
      </Row>
      {/* <Divider></Divider> */}
      <Row justify="center" align="middle">
        <Col span={20}>
          <Row justify="center" align="middle" className="container2">
            <Col span={3}>
              <Text className="text">© 2022 UpSkillLabs</Text>
            </Col>
            <Col span={3} offset={1}>
              <Text className="text">Terms & Conditions</Text>
            </Col>
            <Col span={3} offset={1}>
              <Text className="text">Mentions legales</Text>
            </Col>
            <Col span={3} offset={1}>
              <Space>
                <IconBrandFacebook size={20} color={"#ffff"} />
                <IconBrandYoutube size={20} color={"#ffff"} />
                <IconBrandTwitter size={20} color={"#ffff"} />
                <IconBrandInstagram size={20} color={"#ffff"} />
              </Space>
            </Col>
            <Col span={5} offset={2}>
              <Text className="text">Website Made by Miraiphi Solutions</Text>
            </Col>
          </Row>
        </Col>
      </Row>
    </Content>
  );
}
export default Footer;
