import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";
import { baseURL } from "../../constants";

const { omit } = require("lodash");

/**
 * Helper for the redundant initial state
 */
const getInitialState = () => ({
  user: null,
  userCV: null,
  userRole: null,
  authToken: null,
  loading: false,
});

/**
 * Creates a login session
 */
export const $login = createAsyncThunk(
  "Session/login",
  async ({ email, password }) => {
    const request = {
      method: "post",
      url: `${baseURL}/user/login`,
      data: { email, password },
    };
    const payload = await axios(request);
    return payload.data;
  }
);

/**
 * Creates a login session from existing user or creates a new user from gmail account
 */
export const $googleLogin = createAsyncThunk(
  "Session/googleLogin",
  async (userData) => {
    const request = {
      method: "post",
      url: `${baseURL}/user/googleLogin`,
      data: userData,
    };
    const payload = await axios(request);
    return payload.data;
  }
);

/**
 * Insert user CV (update the user and the cv)
 */
export const $insertUserCV = createAsyncThunk(
  "Session/create",
  async (data) => {
    const request = {
      method: "post",
      url: `${baseURL}/user/insertUserCV`,
      data: data,
    };
    const payload = await axios(request);
    return payload.data;
  }
);
/**
 * Creates a login session
 */
export const $CreateUser = createAsyncThunk("Session/create", async (data) => {
  const request = {
    method: "post",
    url: `${baseURL}/user/create`,
    data: data,
  };
  const payload = await axios(request);
  return payload.data;
});

/**
 * Send email to reset password
 */
export const $forgotPassword = createAsyncThunk(
  "User/forgotPassword",
  async (data) => {
    const request = {
      method: "post",
      url: `${baseURL}/user/forgot-password`,
      data,
    };
    const payload = await axios(request);
    return payload.data;
  }
);

/**
 * create new password
 */
export const $resetPassword = createAsyncThunk(
  "User/resetPassword",
  async (data) => {
    const request = {
      method: "post",
      url: `${baseURL}/user/reset-password`,
      data,
    };
    const payload = await axios(request);
    return payload.data;
  }
);

/**
 *update mentor services
 */
export const $updateMentorServices = createAsyncThunk(
  "User/updateMentorServices",
  async (data) => {
    const request = {
      method: "post",
      url: `${baseURL}/user/updateMentorServices`,
      data,
    };
    const payload = await axios(request);
    return payload.data;
  }
);

/**
 * create and return user role
 */
export const $upsertUserRole = createAsyncThunk(
  "User/upsertUserRole",
  async (data) => {
    const request = {
      method: "post",
      url: `${baseURL}/user/upsertUserRole`,
      data,
    };
    const payload = await axios(request);
    return payload.data;
  }
);
// Session Slice
const Session = createSlice({
  name: "Session",
  initialState: getInitialState(),
  reducers: {
    $logout() {
      const stateUpdate = getInitialState();
      // Clear redux persist storage whatever the scenario
      localStorage.removeItem("persist:root");

      return stateUpdate;
    },
  },
  extraReducers: {
    // Login ($login)
    [$login.fulfilled]: (state, action) => {
      const stateUpdate = { ...state };

      const userData = action.payload;
      axios.defaults.headers.common.Authorization = `Bearer ${userData.user.authToken}`;
      stateUpdate.user = omit(userData?.user, "authToken");
      stateUpdate.authToken = userData?.user.authToken;
      stateUpdate.userCV = userData.cv;
      stateUpdate.userRole = userData.userRole;
      stateUpdate.loading = false;
      return stateUpdate;
    },
    [$login.pending]: (state) => {
      const stateUpdate = { ...state };
      stateUpdate.loading = true;
      return stateUpdate;
    },
    [$login.rejected]: (state) => {
      const stateUpdate = { ...state };
      stateUpdate.loading = false;
      return stateUpdate;
    },

    // google login

    [$googleLogin.fulfilled]: (state, action) => {
      const stateUpdate = { ...state };
      const userData = action.payload;
      axios.defaults.headers.common.Authorization = `Bearer ${userData.authToken}`;
      stateUpdate.user = omit(userData, "authToken");
      stateUpdate.authToken = userData.authToken;
      stateUpdate.loading = false;
      return stateUpdate;
    },
    [$googleLogin.pending]: (state) => {
      const stateUpdate = { ...state };
      stateUpdate.loading = true;
      return stateUpdate;
    },
    [$googleLogin.rejected]: (state) => {
      const stateUpdate = { ...state };
      stateUpdate.loading = false;
      return stateUpdate;
    },

    // create user
    [$CreateUser.fulfilled]: (state, action) => {
      const stateUpdate = { ...state };

      const userData = action.payload;
      axios.defaults.headers.common.Authorization = `Bearer ${userData.authToken}`;

      stateUpdate.user = omit(userData, "authToken");
      stateUpdate.authToken = userData.authToken;

      stateUpdate.loading = false;

      return stateUpdate;
    },

    // update user cv

    [$insertUserCV.fulfilled]: (state, action) => {
      const stateUpdate = { ...state };
      const userData = action.payload;
      stateUpdate.user = userData.updatedUser;
      stateUpdate.userCV = userData.cv;
      stateUpdate.loading = false;
      return stateUpdate;
    },
    [$insertUserCV.pending]: (state) => {
      const stateUpdate = { ...state };
      stateUpdate.loading = true;
      return stateUpdate;
    },
    [$insertUserCV.rejected]: (state) => {
      const stateUpdate = { ...state };
      stateUpdate.loading = false;
      return stateUpdate;
    },

    // update mentor services

    [$updateMentorServices.fulfilled]: (state, action) => {
      const stateUpdate = { ...state };
      const userData = action.payload;
      stateUpdate.user = userData.updatedUser;

      stateUpdate.loading = false;
      return stateUpdate;
    },
    [$updateMentorServices.pending]: (state) => {
      const stateUpdate = { ...state };
      stateUpdate.loading = true;
      return stateUpdate;
    },
    [$updateMentorServices.rejected]: (state) => {
      const stateUpdate = { ...state };
      stateUpdate.loading = false;
      return stateUpdate;
    },

    // update user role

    [$upsertUserRole.fulfilled]: (state, action) => {
      const stateUpdate = { ...state };
      const userData = action.payload;
      stateUpdate.user = userData.user;
      stateUpdate.userRole = userData.role;
      stateUpdate.loading = false;
      return stateUpdate;
    },
    [$upsertUserRole.pending]: (state) => {
      const stateUpdate = { ...state };
      stateUpdate.loading = true;
      return stateUpdate;
    },
    [$upsertUserRole.rejected]: (state) => {
      const stateUpdate = { ...state };
      stateUpdate.loading = false;
      return stateUpdate;
    },
  },
});

export default Session.reducer;

// Simple actions
export const { $logout, forgotPassword, resetPassword, activateAccount } =
  Session.actions;

// Selectors
export const selectSessionUser = (state) => state.Session.user;
export const selectSessionLoading = (state) => state.Session.loading;
export const selectUserCV = (state) => state.Session.userCV;
export const selectUserRole = (state) => state.Session.userRole;
