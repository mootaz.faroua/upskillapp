// packages
import React, { useState } from "react";
import { Button, Col, Row } from "antd";

//styles
import "./index.less";
import { Check } from "tabler-icons-react";

function UserRoles({ userData, onChange }) {
  /* ---------------------------------- HOOKS --------------------------------- */
  // states
  const [changed, setChanged] = useState(0);
  const roles = [
    { label: "Student", value: "student" },
    { label: "Mentor", value: "mentor" },
    { label: "Instructor", value: "instructor" },
    { label: "Recruter ", value: "recruter" },
  ];

  /* -------------------------------- callbacks ------------------------------- */
  function onClickLab(role) {
    userData.role = role.value;
    onChange(userData);
    setChanged(changed + 1);
  }

  /* ---------------------------- rendering helpers --------------------------- */
  function getLabs() {
    return roles?.map((role) => (
      <Row className="roles-row" justify="center">
        <Button className="roles-button" onClick={() => onClickLab(role)}>
          <Row justify="center" align="middle">
            <Col span={18}> {role.label}</Col>
            <Col span={2} offset={2}>
              {userData?.role === role.value ? <Check /> : null}
            </Col>
          </Row>
        </Button>
      </Row>
    ));
  }

  /* -------------------------------- RENDERING ------------------------------- */
  return (
    <div className="user-roles">
      <Row className="introduction-row" justify="center">
        <span>
          Choose your role <br />
        </span>
      </Row>
      <Row justify="center">
        <Col span={24}>{getLabs()}</Col>
      </Row>
    </div>
  );
}

export default UserRoles;
