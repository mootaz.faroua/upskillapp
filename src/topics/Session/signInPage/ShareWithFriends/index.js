// packages
import React, { useState } from "react";
import { Col, Image, Row } from "antd";
import { CopyToClipboard } from "react-copy-to-clipboard";
// assess
import share from "../../../../assets/loginModal/share.png";
import message from "../../../../assets/loginModal/message.png";

//styles
import "./index.less";
import Input from "antd/lib/input/Input";
import { CopyOutlined } from "@ant-design/icons";
import { Send } from "tabler-icons-react";

function ShareWithFriends({ userData, onChange }) {
  // states

  const [value, setValue] = useState();
  const [copied, setCopied] = useState(false);
  /* -------------------------------- callbacks ------------------------------- */
  const onCopyText = () => {
    setCopied(true);
    setTimeout(() => {
      setCopied(false);
    }, 1000);
  };
  /* ---------------------------- rendering helpers --------------------------- */

  /* -------------------------------- rendering ------------------------------- */
  return (
    <div className="share-with-friends">
      <Row className="title-row" justify="center">
        <Col>
          Upskill Labs is the largest community for professionals <br />
          We're sure that your fiends will find it intresting to join
        </Col>
      </Row>
      <Row justify="center">
        <Col>
          <Row justify="center" className="copy-link">
            Copy invitation link
          </Row>
          <Row justify="center">
            <Col span={24}>
              <Input
                suffix={
                  <CopyToClipboard
                    text={value}
                    onCopy={() => {
                      onCopyText();
                    }}
                  >
                    <div className="copied">
                      {copied ? "Copied!" : <CopyOutlined />}
                    </div>
                  </CopyToClipboard>
                }
                value={value}
                onChange={({ target: { value } }) => setValue(value)}
              />
            </Col>
          </Row>
        </Col>
      </Row>
      <Row justify="center" className="invitation-row">
        <Col>Or send Invitation link</Col>
      </Row>
      <Row justify="center">
        <Col span={10}>
          <Image className="image" preview={false} src={share}></Image>
        </Col>
        <Col offset={1} span={10}>
          <Image className="image" preview={false} src={message}></Image>
        </Col>
      </Row>
      <Row justify="center" className="invitation-row">
        <Col>Or invite them by email</Col>
      </Row>
      <Row justify="center" className="input">
        <Col span={14}>
          <Input suffix={<Send color="#ff5300bd" size={15} />} />
        </Col>
      </Row>
    </div>
  );
}

export default ShareWithFriends;
