// packages
import React from "react";
import { Col, Row, Layout } from "antd";

//styles
import "./index.less";
import UploadCV from "../../../Shared/components/uploadCV";

// scoped components
const { Content } = Layout;
function UploadCVPage({ userData, onChange }) {
  /* ---------------------------- rendering helpers --------------------------- */

  /* -------------------------------- rendering ------------------------------- */
  return (
    <Content className="resume-preferences">
      <Row className="resume-row" justify="center">
        <span>
          Want to jump start your career now ? <br />
          Upload your resume and get started
        </span>
      </Row>
      <Row justify="center" className="upload-resume-component">
        <Col span={24}>
          <UploadCV userData={userData} onChange={onChange} />
        </Col>
      </Row>
      {/* <Row justify="center" className="career-lab">
        <Col>Or go to Career Lab and build your CV now</Col>
      </Row>
      <Row justify="center">
        <Button className="button-style" type="primary">
          Build my resume
        </Button>
      </Row> */}
    </Content>
  );
}

export default UploadCVPage;
