// packages
import React, { useEffect, useState } from "react";
import { Button, message, Form, Input, Row, Image, Col } from "antd";
import { GoogleLogin } from "react-google-login";
import { gapi } from "gapi-script";
import FacebookLogin from "react-facebook-login/dist/facebook-login-render-props";

// constants
import { baseURL, strongRegex, validEmail } from "../../../constants";
import axios from "axios";

// components

// assets
import logosignin from "../../../assets/logo2.png";

import loginImage from "../../../assets/modal-img.png";

// constants
import roles from "../../Shared/Entities/roles";
//styles
import "./index.less";
import UserDetails from "./userBasicDetails";
import UserPreferences from "./UserPreferences";
import { $CreateUser, $googleLogin } from "../slice";
import { useDispatch } from "react-redux";
import { $setModalContext } from "../../layouts/slice";
import { useNavigate } from "react-router-dom";
import Layout from "antd/lib/layout/layout";
import { ArrowLeftOutlined, ArrowRightOutlined } from "@ant-design/icons";
import LearningPreferences from "./LearningPreferences";
import ShareWithFriends from "./ShareWithFriends";
import UploadCVPage from "./UploadCVPage";
import UserRoles from "./userRoles";

// Scope components

const { Content } = Layout;
function SignInPage() {
  /* ---------------------------------- HOOKS --------------------------------- */
  //Hooks
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const [form] = Form.useForm();
  // States
  const [userData, setUserData] = useState({ role: "student" });

  const clientId =
    "691444625528-vuhftrv2rlvi2r48n4j1q2tako4ig4vh.apps.googleusercontent.com";
  // side effects
  useEffect(() => {
    const initClient = () => {
      gapi.client.init({
        clientId: clientId,
        scope: "",
      });
    };
    gapi.load("client:auth2", initClient);
  });

  /* -------------------------------- CALLBACKS ------------------------------- */

  function onClickSignIn() {
    dispatch($setModalContext("login"));
  }

  const onSuccess = (res) => {
    const user = {
      firstName: res.profileObj.givenName,
      lastName: res.profileObj.familyName,
      email: res.profileObj.email,
      role: roles.student,
      isForeignUser: true,
    };

    dispatch($googleLogin(user)).then((action) => {
      if (action.error) {
        message.error("login error");
      } else {
        dispatch($setModalContext(null));
        message.success("user logged in successfully");
      }
    });
  };

  const onFacebookSuccess = (res) => {
    const [firstName, lastName] = res.name.split(" ");
    const user = {
      firstName,
      lastName,
      email: res.email,
      role: roles.student,
      isForeignUser: true,
    };

    dispatch($googleLogin(user)).then((action) => {
      if (action.error) {
        message.error("login error");
      } else {
        dispatch($setModalContext(null));
        message.success("user logged in successfully");
      }
    });
  };

  const onFinish = (values) => {
    console.log("values", values);
  };
  /* ----------------------------- RENDER HELPERS ----------------------------- */
  function getSignInFirstPage() {
    return (
      <div className="conatiner">
        <Row className="text-row" justify="center">
          USL is more than an educational platform. Join our community
          <br />
          and join exclusive challenges, rewards and job opportunities.
          <br />
          we can't wait for you to get aboard!
        </Row>
        <Row justify="center" className="text-row">
          <GoogleLogin
            clientId={clientId}
            buttonText="Sign in with Google"
            onSuccess={(e) => {
              onSuccess(e);
            }}
            onFailure={(e) => {
              message.error("Service  not available");
            }}
            cookiePolicy={"single_host_origin"}
            autoLoad={false}
            render={(renderProps) => (
              <Button
                type="primary"
                className="login-button google"
                onClick={renderProps.onClick}
              >
                Sign up with Google
              </Button>
            )}
          />
        </Row>

        <Row justify="center" className="text-row">
          <FacebookLogin
            appId="395198189249942"
            autoLoad={false}
            fields="name,email,picture"
            callback={onFacebookSuccess}
            render={(renderProps) => (
              <Button
                type="primary"
                className="login-button"
                onClick={renderProps.onClick}
              >
                Sign up with Facebook
              </Button>
            )}
          />
        </Row>

        <Row className="text-row" justify="center">
          <span>Or sign up with your email</span>
        </Row>
        <Row className="form-row">
          <Form
            form={form}
            name="basic"
            initialValues={{
              remember: true,
            }}
            onFinish={onFinish}
            autoComplete="off"
          >
            <Row>
              <Col span={11}>
                <Form.Item name="firstName">
                  <Input placeholder="Firstname" />
                </Form.Item>
              </Col>
              <Col offset={1} span={11}>
                <Form.Item name="lastName">
                  <Input placeholder="Lastname" />
                </Form.Item>
              </Col>
            </Row>
            <Row>
              <Col span={11}>
                <Form.Item
                  name="email"
                  rules={[
                    {
                      required: true,
                      message: "Please input your email!",
                    },

                    () => ({
                      validator(_, value) {
                        if (!value || value.match(validEmail)) {
                          return Promise.resolve();
                        }
                        return Promise.reject(
                          new Error("The email format does not match")
                        );
                      },
                    }),
                  ]}
                >
                  <Input placeholder="Email" />
                </Form.Item>
              </Col>
              <Col span={11} offset={1}>
                <Form.Item
                  name="password"
                  rules={[
                    {
                      required: true,
                      message: "Please input your password!",
                    },
                    () => ({
                      validator(_, value) {
                        if (!value || value.match(strongRegex)) {
                          return Promise.resolve();
                        }
                        return Promise.reject(
                          new Error(
                            "The password does not match the requirement"
                          )
                        );
                      },
                    }),
                  ]}
                >
                  <Input.Password placeholder="Password" />
                </Form.Item>
              </Col>
            </Row>
          </Form>
        </Row>
        <Row className="conditions-row">
          By clicking get started I agree to the terms and conditions
        </Row>
      </div>
    );
  }
  // LocalVariables
  const steps = [
    {
      title: "",
      content: getSignInFirstPage(),
    },

    {
      title: "",
      content: <UserRoles userData={userData} onChange={setUserData} />,
    },
    {
      title: "",
      content: <UserPreferences userData={userData} onChange={setUserData} />,
    },
    {
      title: "",
      content: (
        <div className="user-details-container">
          <UserDetails userData={userData} onChange={setUserData} />
        </div>
      ),
    },

    {
      title: "",
      content: (
        <LearningPreferences userData={userData} onChange={setUserData} />
      ),
    },
    {
      title: "",
      content: <UploadCVPage userData={userData} onChange={setUserData} />,
    },
    {
      title: "",
      content: <ShareWithFriends userData={userData} onChange={setUserData} />,
    },
  ];

  // uplaodCV
  // On file upload (click the upload button)
  async function fileUpload(userId) {
    // Create an object of formData
    const data = new FormData();

    // Update the formData object
    data.append("file", userData.cv);
    data.set("userId", userId);

    axios({
      method: "post",
      baseURL: `${baseURL}/user/uploadCV`,
      data,
      body: data,
    })
      .then((response) => {
        message.success(response.data.message);
      })
      .catch((error) => {
        message.error(error);
      });
  }

  // on click Done
  async function createUser() {
    const userInfo = userData.role
      ? userData
      : { ...userData, role: roles.student };
    dispatch($CreateUser(userInfo)).then((action) => {
      if (action.error) {
        message.error(
          "login error"
          //@Todo:  Add translation to error
        );
      } else {
        dispatch($setModalContext(null));

        fileUpload(action.payload._id);
        message.success("user logged in successfully");
        if (action?.payload?.roles?.includes(roles.recruter)) {
          navigate("/recruterPage");
        }
         else if (action?.payload?.roles?.includes(roles.instructor)) {
          navigate("/learning");
      }
      else if (action?.payload?.roles?.includes(roles.mentor)) {
        navigate("/mentorLab");
       }
       else if (action?.payload?.roles?.includes(roles.student)) {
        navigate("/displaycourse");
    }
      }
    });
  }

  const [current, setCurrent] = useState(0);

  const next = () => {
    if (current === 0) {
      form
        .validateFields()
        .then((values) => {
          const user = form.getFieldsValue();
          setUserData({ ...userData, ...user });
          setCurrent(current + 1);
        })
        .catch((errors) => console.error("ERRORS", errors));
    } else {
      setCurrent(current + 1);
    }
  };

  const prev = () => {
    setCurrent(current - 1);
  };

  const skip = () => {
    setCurrent(steps.length - 1);
  };
  /* -------------------------------- RENDERING ------------------------------- */
  return (
    <Content className="main-container">
      <Content className="image-container">
        <Image className="login-image" preview={false} src={loginImage}></Image>
      </Content>
      <Content className="form-container">
        <Row className="logorow">
          <Image
            className="logo-signup"
            preview={false}
            src={logosignin}
          ></Image>
        </Row>
        <Content className="steps-content">{steps[current].content}</Content>
        <Content className="steps-action">
          <Row justify="center" className="get-started-button">
            {current < steps.length - 1 && (
              <Col span={11}>
                <Button
                  className="Button-Get-Started"
                  type="primary"
                  htmlType="submit"
                  style={{
                    width: "100%",
                    borderRadius: "10px",
                    fontWeight: "600",
                    fontSize: "1em",
                    borderColor: "#fe9816",
                    background: "#fe9816",
                  }}
                  onClick={() => next()}
                >
                  {current === 0 ? "Get started" : "Continue"}
                </Button>
              </Col>
            )}
          </Row>
          <Row justify="center">
            {current === steps.length - 1 && (
              <Col offset={1} span={5}>
                <Button
                  className="continue-button-style"
                  type="primary"
                  onClick={() => createUser()}
                >
                  Done
                </Button>
              </Col>
            )}
          </Row>
          <Row>
            {current > 0 && (
              <Col span={3}>
                <div className="steps-button-style" onClick={() => prev()}>
                  <ArrowLeftOutlined />
                  {`  Back`}
                </div>
              </Col>
            )}
            <Col span={3} offset={17}>
              {current > 0 && current !== steps.length - 1 && (
                <div className="steps-button-style" onClick={() => skip()}>
                  {`skip  `} <ArrowRightOutlined />
                </div>
              )}
            </Col>
          </Row>

          {current === 0 && (
            <Row justify="center" className="signup-row">
              <Col offset={7} span={18}>
                <span>Already have an account?</span>
                <span
                  className="signup-button"
                  onClick={() => {
                    onClickSignIn();
                  }}
                >
                  {`   Sign in`}
                </span>
              </Col>
            </Row>
          )}
        </Content>
      </Content>
    </Content>
  );
}

export default SignInPage;
