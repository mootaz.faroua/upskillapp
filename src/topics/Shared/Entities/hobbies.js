export const hobbies = [
  { value: "sport", label: "Sport" },
  { value: "music", label: "Music" },
  { value: "travelling", label: "Travelling" },
  { value: "dance", label: "Dance" },
  { value: "cooking", label: "Cooking" },
  { value: "painting", label: "Painting" },
  { value: "singing", label: "Singing" },
  { value: "acting", label: "Acting" },
];
