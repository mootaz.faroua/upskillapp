import React, { useState } from "react";
import { IconTrash } from "@tabler/icons";
import { omit } from "lodash";
import { Col, Layout, Row } from "antd";
import "./index.less";
import { Upload } from "tabler-icons-react";

// scoped components
const { Content } = Layout;
/* ------------------------------- Components ------------------------------- */
function UploadCV({ userData, onChange }) {
  // state
  const [selectedFile, setSelectedFile] = useState(undefined);
  // On file select (from the pop up)
  const onFileChange = (event) => {
    // Update the state
    setSelectedFile(event.target.files[0]);
    userData.cv = event.target.files[0];
    onChange(userData);
  };

  // file upload is complete
  const fileData = () => {
    if (selectedFile) {
      return (
        <Row className="selected-file" justify="center">
          <Col className="selected-file-name"> {selectedFile.name}</Col>
          <Col>
            <IconTrash
              color="orange"
              size={20}
              onClick={() => {
                setSelectedFile();
                onChange(omit(userData, "cv"));
              }}
            />
          </Col>
        </Row>
      );
    } else {
      return <Row></Row>;
    }
  };

  return (
    <Content className="resume-container">
      <Row className="resume-upload-container" justify="center">
        <Col>
          <label className="label">
            <Row className="row">
              <Col>
                <div className="upload-text">{`Drop file here   `}</div>
              </Col>

              <Col>
                <Upload color="orange" size={15} />
              </Col>
            </Row>
            <br />

            <input
              className="inputTag"
              type="file"
              onChange={(e) => {
                onFileChange(e);
              }}
              name="file"
              accept="application/msword, .doc, .docx, application/pdf"
            />
            <br />
            <span id="imageName"></span>
          </label>
        </Col>
      </Row>
      {fileData()}
    </Content>
  );
}

export default UploadCV;
