/* -------------------------------------------------------------------------- */
/*                                Dependencies                                */
/* -------------------------------------------------------------------------- */

// Packages
import React from "react";

// UI lib components
import { Layout, Spin } from "antd";

// Local images
import LOADER from "../../../../assets/logo.png";

// Style
import "./index.less";

/* -------------------------------------------------------------------------- */
/*                                Loader Component                            */
/* -------------------------------------------------------------------------- */

function Loader() {
  /* ***************************** RENDER HELPERS ***************************** */
  const LOADER_ICON = (
    <img className="loader-icon" src={LOADER} alt="loader"  />
  );
  /* ******************************** RENDERING ******************************* */
  return (
    <Layout className="loader-layout">
      <Layout.Content>
        <Spin className="loader-container" indicator={LOADER_ICON} />
      </Layout.Content>
    </Layout>
  );
}

export default Loader;
